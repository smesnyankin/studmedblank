/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @since       3.2
 */

(function($) {
	$(document).ready(function() {
	    $("#carousel1").owlCarousel({
		//autoPlay: 3000, //Set AutoPlay to 3 seconds
		items : 3,
		itemsDesktop : [1199,3],
		itemsDesktopSmall : [979,2],
		itemsTablet : [640,1]
	    
	    });
        
        $(".next").click(function(){
            $("#carousel1").trigger('owl.next');
        })
        $(".prev").click(function(){
            $("#carousel1").trigger('owl.prev');
        })
        
	    jQuery("img.svg").each(function(){
		var $img = jQuery(this);
		var imgID = $img.attr('id');
		var imgClass = $img.attr('class');
		var imgURL = $img.attr('src');
		
		jQuery.get(imgURL, function(data) {
			// Get the SVG tag, ignore the rest
			var $svg = jQuery(data).find('svg');
			//console.log( $svg.text());
			// Add replaced image's ID to the new SVG
			if(typeof imgID !== 'undefined') {
				$svg = $svg.attr('id', imgID);
			}
			// Add replaced image's classes to the new SVG
			if(typeof imgClass !== 'undefined') {
				$svg = $svg.attr('class', imgClass+' replaced-svg');
			}
			// Remove any invalid XML tags as per http://validator.w3.org
			$svg = $svg.removeAttr('xmlns:a');
			// Replace image with new SVG
			//$mysvg = 
			$img.replaceWith($svg);
		}, 'xml');
	    });
	    $('.search-container input[type="submit"]').click(function(e){
		
		var container = $(this).parents('.adaptive-menu');
		var menu = container.find('.top-menu');
		var search = container.find('.search-container');
		
		console.log(search.hasClass('active'));
		if (!search.hasClass('active')) {
		    search.addClass('active');

		    console.log(container.width() + ' ' + menu.width() + ' ' + search.width());
		    if (container.width() < menu.width() + 310) { // + 20px + 20px
			menu.addClass('compact');
		    } else {
			menu.removeClass('compact');
		    }
		    e.preventDefault();
		}
	    });
	    
	    $('.search-container input[type="text"]').blur(function() { //.moduletable.search-container.active 
		var container = $('.adaptive-menu');
		var menu = container.find('.top-menu');
		var search = container.find('.search-container');
		
		setTimeout(function(){ 
		    search.removeClass('active');
		    menu.removeClass('compact');
		}, 100);
	    });
		$('*[rel=tooltip]').tooltip()
		$('.adaptive-menu .menu-button').click(function(){
		    var parent = $(this).parent();
		    if (parent.hasClass('compact')) {
			parent.removeClass('compact');
		    } else
			$(this).toggleClass('active');
		});
		// Turn radios into btn-group
		$('.radio.btn-group label').addClass('btn');
		$(".btn-group label:not(.active)").click(function()
		{
			var label = $(this);
			var input = $('#' + label.attr('for'));

			if (!input.prop('checked')) {
				label.closest('.btn-group').find("label").removeClass('active btn-success btn-danger btn-primary');
				if (input.val() == '') {
					label.addClass('active btn-primary');
				} else if (input.val() == 0) {
					label.addClass('active btn-danger');
				} else {
					label.addClass('active btn-success');
				}
				input.prop('checked', true);
			}
		});
		$(".btn-group input[checked=checked]").each(function()
		{
			if ($(this).val() == '') {
				$("label[for=" + $(this).attr('id') + "]").addClass('active btn-primary');
			} else if ($(this).val() == 0) {
				$("label[for=" + $(this).attr('id') + "]").addClass('active btn-danger');
			} else {
				$("label[for=" + $(this).attr('id') + "]").addClass('active btn-success');
			}
		});
        
        $('body').on('click', '.open-popup', function(){
            $('.popup-window-overlay').fadeIn(300);
            $('.popup-window').fadeIn(300, function(){
                $('body').css("overflow-x", "hidden")
            });
            return false;
        });
        $('body').on('click', '.popup-window-close, .popup-window-overlay', function(){
            $('.popup-window-overlay').fadeOut(300);
            $('.popup-window').fadeOut(300, function(){
                $('body').css("overflow-x", "visible")
            });
            return false;
            
        });
        
	})
})(jQuery);