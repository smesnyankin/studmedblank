<?php

function debug($data, $stop=false){
    echo '<pre>';
    print_r($data);
    echo '<pre>';
    if($stop){
    	exit();	
    }
}

//генерация временного пароля
function generatePassword ($length = 8) {
	$password = "";
 	$possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ"; 
 	$maxlength = strlen($possible); 
 	if ($length > $maxlength){
 		$length = $maxlength;
 	} 
	$i = 0;
 	while ($i < $length) { 
 		$char = substr($possible, mt_rand(0, $maxlength-1), 1); 
 		if (!strstr($password, $char)) {
 			$password .= $char; 
 			$i++;
 		}
 	}  
 return $password;
}

//генерация токена для авторизации
function generateToken(){
	return md5(uniqid(rand(), true));
}


function loginUser($token, $path = '/'){
	setcookie('auth_token', $token, time()+app::gi()->config->cookietime, $path);
}

function logoutUser($path = '/'){
	setcookie('auth_token', '', time()-1, $path);
}


/**
 * Уставнавливает куки для выбраного региона
 * 
 * строка вида id_country:id_region:id_city
 * например 43:657:67
 * @return boolean
 **/
function setCookieLocation($id_city = 0, $id_region = 0, $id_country = 0){
	$string = $id_city .':' . $id_region . ':' . $id_country;
	setcookie('region', $string, time() + app::gi()->config->cookietime, '/');
	return isset($_COOKIE['region']) ? true : false;
}

/**
 * Парсит куку хранящую информацию о регионе
 * @return array
 **/
function getCookieLocation(){
	if(isset($_COOKIE['region']) && !empty($_COOKIE['region'])){
		$region = explode(':', $_COOKIE['region']);
		if(count($region) == 3){
			return array('id_city' => (int)$region[0], 'id_region' => (int)$region[1], 'id_country' => (int)$region[2]);
		}
	}
	return false;
}

//возвращает переод по ключу из массива
function lang($key){
	return array_key_exists($key, app::gi()->translate) ? app::gi()->translate[$key] : '';
}