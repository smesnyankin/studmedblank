
<h2>Добавление статьи</h2>
<script src="/cp/assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
	tinymce.init({
            selector: "#cont",
            language: "ru",
            height: 200,
            
		    plugins: [
		         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
		         "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
		         "table contextmenu directionality emoticons paste textcolor responsivefilemanager"
		   ],
		   toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
		   toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
		   image_advtab: true ,
	   
		   external_filemanager_path:"/cp/core/libs/filemanager/",
		   filemanager_title:"Responsive Filemanager" ,
		   external_plugins: { "filemanager" : "/cp/core/libs/filemanager/plugin.min.js"}
        });
</script>
<?php if($page){ ?>
<form method="post">
	Вопрос:<br>
	<input type="text" name="form[title]" value="<?=(!empty($_POST)) ? $_POST['form']['title'] : ''; ?>"><br>
	Описание:<br>
        <textarea id="cont" name="form[text]"><?=(!empty($_POST)) ? $_POST['form']['text'] : ''; ?></textarea><br>
	URL на страницу справки:<br>
	<input type="text" name="form[go_url]" value="<?=(!empty($_POST)) ? $_POST['form']['go_url'] : ''; ?>"><br>
        Страница:<br>
        <select name="form[id_page]">
            <!--<option value="-1">Выбрать</option>-->
            <?php // foreach ($pages as $page): ?>
                <option value="<?= $page->id; ?>"><?= $page->title; ?></option>
            <?php //endforeach; ?>
        </select>
	<input type="submit" value="Сохранить">
</form>
    
    <?php if (count($exists)){ ?>
    <p>Существующие статьи, для страницы: <?=$page->title ? $page->title : ''; ?></p>
        <table>
            <tr>
                <td>Вопрос</td>
                <td>Описание</td>
                <td>Редактировать</td>
                <td>Удалить</td>
            </tr>
            <?php foreach ($exists as $post){ ?>
            <tr>
                <td><?=$post->title; ?></td>
                <td><?=$post->text; ?></td>
                <td><a href="/cp/content/statiedit/<?= $post->id; ?>">[редактировать]</a></td>
                <td><a href="/cp/content/statidelete/<?= $post->id.'/'.$page->id; ?>" onclick="if (confirm('Вы уверены, что хотите удалить пост?'))
                            location.href = '/cp/content/reviewdelete/<?= $post->id.'/'.$page->id; ?>';">[удалить]</a></td>
            </tr>
            <?php } ?>
        </table>
    <?php } ?>
<?php } else { ?>
    <p>Для добавления вопроса-ответа необходимо создать хотя бы одну страницу вопросов-ответов</p>
<?php } ?>