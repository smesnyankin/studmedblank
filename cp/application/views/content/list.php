<h2>Статьи</h2>
<?php $count_articles = false; ?>
<?php if (count($categories)): ?>
    <?php foreach ($categories as $category): ?>
        <?php if (count($category->articles)): ?>
            <?php $count_articles = true; ?>
            <h3><?= $category->title; ?></h3>
            <table>
                <tr>
                    <td>Название</td>
                    <td>Url</td>
                    <td>Редактировать</td>
                    <td>Удалить</td>
                </tr>
                <?php foreach ($category->articles as $article): ?>
                    <tr>
                        <td><a href="/stati/<?= $article->url; ?>" target="_blank"><?= $article->title; ?></a></td>
                        <td><?= $article->url; ?></td>
                        <td><a href="/cp/content/edit/<?= $article->id; ?>">[редактировать]</a></td>
                        <td><a href="#" onclick="if (confirm('Вы уверены, что хотите удалить статью?'))
                                    location.href = '/cp/content/delete/<?= $article->id; ?>';">[удалить]</a></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>
<?php if (!$count_articles): ?>
    <p>Пока что не добавлено ни одной статьи</p>
<?php endif; ?>