<?php if ($rule): ?>
    <h2>Фото "<?= $rule->spravka_title; ?>"</h2>
    <!-- Расширенные поля заказа справки:<br> -->
    <h3>Изображение справки</h3>
    <div class="container">
        <?php if ($rule->img): ?>
            Загруженное изображение:<br>
            <img src="/uploads/rules/<?=$rule->img;?>" width="250" height="300">
        <?php else: ?>
            Нет изображения
        <?php endif; ?>
    </div>
    <div class="container">
        <form enctype="multipart/form-data"  method="POST">
            Загрузка изображения: <input name="userfile" type="file" />
            <input type="submit" value="Загрузить" />
        </form>
    </div>
<?php endif; ?>

<?php if (count($exists)){ ?>
<p>Существующие правила заполненния</p>
    <table>
        <tr>
            <td>Название справки</td>
            <td>Заголовок правила</td>
            <td>Картинка</td>
            <td>Изменить картинку</td>
            <td>Удалить</td>
        </tr>
        <?php foreach ($exists as $post){ ?>
        <tr>
            <td><?=$post->spravka_title; ?></td>
            <td><?=$post->rule_title; ?></td>
            <td>
            <?php if ($post->img): ?>
                <img src="/uploads/rules/<?=$post->img;?>" width="100" height="100">
            <?php else: ?>
                Нет изображения
            <?php endif; ?>
            </td>
            <td><a href="/cp/content/additional/<?= $post->id; ?>">[изменить картинку]</a></td>
            <td><a href="/cp/content/ruledelete/<?= $post->id.'/'.$post->id_page; ?>" onclick="if (confirm('Вы уверены, что хотите удалить пост?'))
                        location.href = '/cp/content/ruledelete/<?= $post->id.'/'.$post->id_page; ?>';">[удалить]</a></td>
        </tr>
        <?php } ?>
    </table>
<?php } ?>