<?php if ($stati): ?>
    <h2>Фото "<?= $stati->title; ?>"</h2>
    <!-- Расширенные поля заказа справки:<br> -->
    <h3>Изображение справки</h3>
    <div class="container">
        <?php if ($stati->img): ?>
            Загруженное изображение:<br>
            <img src="/uploads/stati/<?=$stati->img;?>" width="250" height="300">
        <?php else: ?>
            Нет изображения
        <?php endif; ?>
    </div>
    <div class="container">
        <form enctype="multipart/form-data"  method="POST">
            Загрузка изображения: <input name="userfile" type="file" />
            <input type="submit" value="Загрузить" />
        </form>
    </div>
<?php endif; ?>

<?php if (count($exists)){ ?>
    <p>Существующие сатьи на станице: <h2><?=$page->title; ?></h2></p>
    <table>
        <tr>
            <td>Вопрос</td>
            <td>URL на справку</td>
            <td>Картинка</td>
            <td>Изменить картинку</td>
            <td>Удалить</td>
        </tr>
        <?php foreach ($exists as $post){ ?>
        <tr>
            <td><?=$post->title; ?></td>
            <td><?=$post->go_url; ?></td>
            <td>
            <?php if ($post->img): ?>
                <img src="/uploads/stati/<?=$post->img; ?>" width="100" height="100">
            <?php else: ?>
                Нет изображения
            <?php endif; ?>
            </td>
            <td><a href="/cp/content/additionalstati/<?= $post->id; ?>">[изменить картинку]</a></td>
            <td><a href="/cp/content/statidelete/<?= $post->id.'/'.$post->id_page; ?>" onclick="if (confirm('Вы уверены, что хотите удалить пост?'))
                        location.href = '/cp/content/statidelete/<?= $post->id.'/'.$post->id_page; ?>';">[удалить]</a></td>
        </tr>
        <?php } ?>
    </table>
<?php } ?>