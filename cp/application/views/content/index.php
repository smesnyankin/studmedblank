<?php if ($categories): ?>
    <script src="/cp/assets/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
    tinymce.init({
        selector: "#content",
        language: "ru",
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons paste textcolor responsivefilemanager"
        ],
        toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
        toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
        image_advtab: true,
        external_filemanager_path: "/cp/core/libs/filemanager/",
        filemanager_title: "Responsive Filemanager",
        external_plugins: {"filemanager": "/cp/core/libs/filemanager/plugin.min.js"}
    });
    </script>
    <h2><?= $content->id ? 'Редактирование' : 'Новая статья'; ?></h2>
    <form enctype="multipart/form-data"  method="POST">
        Url (только латиница):<br>
        <input type="text" name="form[url]" value="<?= $content->id ? $content->url : ''; ?>" required>
        meta_title:<br>
        <input type="text" name="form[meta_title]" value="<?= $content->id ? $content->meta_title : ''; ?>">
        meta_keywords:<br>
        <input type="text" name="form[meta_keywords]" value="<?= $content->id ? $content->meta_keywords : ''; ?>">
        meta_description:<br>
        <input type="text" name="form[meta_description]" value="<?= $content->id ? $content->meta_description : ''; ?>">
        Заголовок:<br>
        <input type="text" name="form[title]" value="<?= $content->id ? $content->meta_description : ''; ?>" required>
        Категория:
        <select name="form[id_category]">
            <?php foreach ($categories as $category): ?>
                <option <?= $category->id == $content->id_category; ?> value="<?= $category->id; ?>"><?= $category->title; ?></option>
            <?php endforeach; ?>
        </select>


        <h3>Превью справки</h3>
        <div class="container">
                изображение: <input name="userfile" type="file" />
        </div>
        <br>

        Контент: <br>
        <textarea id="content" name="form[content]"><?=$content->content;?></textarea><br>
        <input type="submit" value="<?= $content->id ? 'Сохранить' : 'Добавить' ?>">
    </form>
<?php else: ?>
    <p>Необходимо создать хотя бы одну категорию</p><br>
    <a href="/cp/category">[создать]</a>
<?php endif; ?>
