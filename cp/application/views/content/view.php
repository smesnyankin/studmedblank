<?php if ($content): ?>
    <h2>Просмотр статьи</h2>
    Ссылка статьи на сайте: <a href="/stati/<?= $content->url; ?>"><?= $content->url; ?></a><br>
    Заголовок: <?= $content->title; ?><br>
    Контент: <?= $content->content; ?>
<?php endif; ?>