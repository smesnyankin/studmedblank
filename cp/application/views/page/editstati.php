<h2>Редактирование страницы статей</h2>

    <script src="/cp/assets/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
        tinymce.init({
            selector: "#content",
            language: "ru",
            height: "100",
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons paste textcolor responsivefilemanager"
            ],
            toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
            toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
            image_advtab: true,
            external_filemanager_path: "/cp/core/libs/filemanager/",
            filemanager_title: "Responsive Filemanager",
            external_plugins: {"filemanager": "/cp/core/libs/filemanager/plugin.min.js"}
        });
    </script>
    <form action ="" method="post">
        meta_title:<br>
	<input type="text" name="form[meta_title]" value="<?=$page->meta_title ? $page->meta_title : ''; ?>"><br>
        meta_keywords:<br>
	<input type="text" name="form[meta_keywords]" value="<?=$page->meta_keywords ? $page->meta_keywords : ''; ?>"><br>
        meta_description:<br>
	<input type="text" name="form[meta_description]" value="<?=$page->meta_description ? $page->meta_description : ''; ?>"><br>
        Заголовок:<br>
	<textarea id="content" name="form[title]" ><?=$page->title ? $page->title : ''; ?></textarea><br>
	URL:<br>
        <input type="text" name="form[url]" value="<?=$page->url ? $page->url : ''; ?>" required ><br>
        <input type="submit" value="Редактировать">
    </form>
    
    <?php if (count($exists)): ?>
        <p>Существующие страницы статей:</p>
        <table>
            <tr>
                <td>Загловок</td>
                <td>URL</td>
                <td>редактировать</td>
                <td>перейти к добавлению постов</td>
            </tr>
            <?php foreach ($exists as $exist){ ?>
            <tr>
                <td><?=$exist->title; ?></td>
                <td><?=$exist->url; ?></td>
                <td><a href="/cp/page/editstati/<?= $exist->id; ?>">[редактировать]</a></td>
                <td><a href="/cp/content/stati/<?= $exist->id; ?>">[добавить посты]</a></td>
            </tr>
            <?php } ?>
        </table>
    <?php endif; ?>