<h2>Категории</h2>
<h3><?= $category->id ? 'Редактирование категории' : 'Новая категория'; ?></h3>
<form method="post">
    Название:<br>
    <input type="text" name="form[title]" value="<?= $category->id ? $category->title : ''; ?>" required>
    Url (только латиница):<br>
    <input type="text" name="form[url]" value="<?= $category->id ? $category->url : ''; ?>" required>
    <input type="submit" value="<?= $category->id ? 'Сохранить' : 'Создать'; ?>">
</form>
<div>
    <?php if ($cats): ?>
        <table>
            <tr>
                <td>Название</td>
                <td>Url</td>
                <td>редактировать</td>
                <td>удалить</td>
            </tr>
            <?php foreach ($cats as $cat): ?>
                <tr>
                    <td><?= $cat->title; ?></td>
                    <td><?= $cat->url; ?></td>
                    <td><a href="/cp/category/index/<?= $cat->id; ?>">[редактировать]</a></td>
                    <td><a href="#" onclick="if (confirm('Вы уверены, что хотите удалить категрию?'))
                                location.href = '/cp/category/delete/<?= $cat->id; ?>';">[удалить]</a></td>
                </tr>
        <?php endforeach; ?>
        </table>
<?php endif; ?>
</div>