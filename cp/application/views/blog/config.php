<?php $day = 3600*24; $hour = 3600; ?>

<h2>Редактирование конфигурации автопостинга</h2> 
    <form action ="" method="post">
	Интервал добавления:<br>
        <p>
            Дни:
            <select name="form[day]">
                <?php for($i=0;$i<32;$i++){ ?>                
                <option <?= ((int)($config->intervall / $day) == $i) ? 'selected' : ''; ?> value="<?=$i; ?>"><?=$i; ?></option>
                <?php } ?>
            </select>
            Часы:
            <select name="form[hour]">
                <?php for($i=0;$i<24;$i++){ ?>
                    <option <?= ((int)($config->intervall % $day/ $hour) == $i) ? 'selected' : ''; ?> value="<?=$i; ?>"><?=$i; ?></option>
                <?php } ?>
            </select>
        </p>
        
        Количество добавляемых статей за раз:
        <select name="form[post_count]">
            <?php for($i=1;$i<21;$i++){ ?>
                <option <?= (int)($config->post_count == $i) ? 'selected' : ''; ?> value="<?=$i; ?>"><?=$i; ?></option>
            <?php } ?>
        </select>
	
        <input type="submit" value="Редактировать">
    </form>

Установленный интервал в часах(информация не для редактирования):
    <input type="text" value="<?=($config->intervall) ? $config->intervall/3600 : ''; ?>" required><br> 
