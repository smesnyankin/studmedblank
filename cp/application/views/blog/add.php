<h2>Добавление поста для автопостинга</h2>
<script src="/cp/assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
	tinymce.init({
            selector: "#content",
            language: "ru",
            height: 200,
            
		    plugins: [
		         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
		         "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
		         "table contextmenu directionality emoticons paste textcolor responsivefilemanager"
		   ],
		   toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
		   toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
		   image_advtab: true ,
	   
		   external_filemanager_path:"/cp/core/libs/filemanager/",
		   filemanager_title:"Responsive Filemanager" ,
		   external_plugins: { "filemanager" : "/cp/core/libs/filemanager/plugin.min.js"}
        });
</script>
<form method="post">
        meta_title:<br>
	<input type="text" name="form[meta_title]"><br>
        meta_keywords:<br>
        <input type="text" name="form[meta_keywords]"><br>
        meta_description:<br>
	<input type="text" name="form[meta_description]"><br>
        Заголовок:<br>
	<input type="text" name="form[title]" ><br>
        URL на статью (транслит Заголовка):<br>
	<input type="text" name="form[go_url]" ><br>
        Текст:<br>
	<textarea id="content" name="form[text]"></textarea><br>
	<input type="submit" value="Сохранить">
</form>

<?php if ($exists){ ?>
    <p>Существующие посты для автопостинга: </p>
    <table>
        <tr>
            <td>Загловок</td>
            <td>редактировать</td>
            <!--<td>удалить</td>-->
        </tr>
        <?php foreach ($exists as $post){ ?>
        <tr>
            <td><?=$post->title; ?></td>
            <td><a href="/cp/blog/edit/<?= $post->id; ?>">[редактировать]</a></td>
        </tr>
        <?php } ?>
    </table>
<?php } ?>
