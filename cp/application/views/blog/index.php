<h2>Существующие посты для автопостинга:</h2>
<?php if (count($exists)): ?>
        <table>
            <tr>
                <td>Заголовок</td>
                <td>URL на статью</td>
                <td>Редактировать</td>
                <td>удалить</td>
            </tr>
        <?php foreach ($exists as $post): ?>
            <tr>
                <td><?= $post->title; ?></td>
                <td><?= $post->go_url; ?></td>
                <td><a href="/cp/blog/edit/<?= $post->id; ?>">[редактировать]</a></td>
                <td><a href="/cp/blog/delete/<?= $post->id; ?>" onclick="if (confirm('Вы уверены, что хотите удалить пост?'))
                            location.href = '/cp/blog/delete/<?= $post->id; ?>';">[удалить]</a></td>
            </tr>
        <?php endforeach; ?>
        </table>
<?php else: ?>
    <p>Пока еще не добавлено ни одного поста</p>
<?php endif; ?>