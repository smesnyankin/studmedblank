
<h3>Изображение для :  <?=$post->title; ?></h3>
<h3>Изображение справки</h3>
    <div class="container">
        <?php if ($post->img): ?>
            Загруженное изображение:<br>
            <img src="/uploads/blog/<?=$post->img;?>" width="200" height="200">
        <?php else: ?>
            Нет изображения
        <?php endif; ?>
    </div>
<div class="container">
    <form action="" enctype="multipart/form-data"  method="POST">
        Загрузка изображения: <input name="userfile" type="file" />
        <input type="submit" value="Загрузить" />
    </form>
</div>

<?php if ($exists){ ?>
        <p>Существующие посты автопостинга:
        <table>
            <tr>
                <td>Заголовок</td>
                <td>URL на статью</td>
                <td>IMG</td>
                <td>редактировать</td>
                <td>удалить</td>
            </tr>
            <?php foreach ($exists as $post){ ?>
            <tr>
                <td><?=$post->title; ?></td>
                <td><?=$post->go_url; ?></td>
                <td>
                    
                    <?php if ($post->img) { ?>
                        <img width="50" height="50" src="/uploads/blog/<?=$post->img; ?>">
                    <?php } else {?>
                        <p>Отсутствует</p>
                    <?php } ?>
                    
                </td>
                <td><a href="/cp/blog/edit/<?=$post->id; ?>">[редактировать]</a></td>
                <td><a href="/cp/blog/delete/<?=$post->id; ?>" onclick="if (confirm('Вы уверены, что хотите удалить пост?'))
                            location.href = '/cp/blog/delete/<?= $post->id; ?>';">[удалить]</a></td>
            </tr>
            <?php } ?>
        </table>
    <?php } ?>