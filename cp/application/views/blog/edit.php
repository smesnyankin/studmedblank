<h2>Редактирование поста для автопостинга</h2> 
<p>
    <b>Фото</b>: <a href="/cp/blog/additional/<?=$post->id;?>">[редактировать]</a>
</p>

<script src="/cp/assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
	tinymce.init({
            selector: "#content",
            language: "ru",
            height: 100,
            
		    plugins: [
		         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
		         "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
		         "table contextmenu directionality emoticons paste textcolor responsivefilemanager"
		   ],
		   toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
		   toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
		   image_advtab: true ,
	   
		   external_filemanager_path:"/cp/core/libs/filemanager/",
		   filemanager_title:"Responsive Filemanager" ,
		   external_plugins: { "filemanager" : "/cp/core/libs/filemanager/plugin.min.js"}
        });
</script>
    <form action ="" method="post">
        meta_title:<br>
	<input type="text" name="form[meta_title]" value="<?=($post->meta_title) ? $post->meta_title : ''; ?>"><br>
        meta_keywords:<br>
	<input type="text" name="form[meta_keywords]" value="<?=($post->meta_keywords) ? $post->meta_keywords : ''; ?>"><br>
        meta_description:<br>
	<input type="text" name="form[meta_description]" value="<?=($post->meta_description) ? $post->meta_description : ''; ?>"><br>
	Заголовок:<br>
	<input type="text" name="form[title]" value="<?=($post->title) ? $post->title : ''; ?>" required><br> 
	URL на статью (транслит Заголовка):<br>
	<input type="text" name="form[go_url]" value="<?=($post->go_url) ? $post->go_url : ''; ?>" required><br> 
        Текст:<br>
	<textarea id="content" name="form[text]"><?=($post->text) ? $post->text : ''; ?></textarea><br>
        <input type="submit" value="Редактировать">
    </form>
    
    <?php if (count($exists)): ?>
        <p>Существующие посты для автопостинга: </p>
        <table>
            <tr>
                <td>Загловок</td>
                <td>редактировать</td>
            </tr>
            <?php foreach ($exists as $post){ ?>
            <tr>
                <td><?=$post->title; ?></td>
                <td><a href="/cp/blog/edit/<?= $post->id; ?>">[редактировать]</a></td>
            </tr>
            <?php } ?>
        </table>
    <?php endif; ?>
