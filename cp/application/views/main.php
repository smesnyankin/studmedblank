<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= app::gi()->config->sitename; ?></title>
    <link rel="stylesheet" href="/cp/assets/css/adminstyle.css">
</head>
<body>
   <?php include dirname(__FILE__) . '/layouts/' . $this->layout . '.php'; ?>
</body>
</html>
