<?php

class ContentController extends Controller {

    static $rules = array(
        'index' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'create' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'list' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'edit' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'faq' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'faqdelete' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'faqedit' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'view' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'review' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'reviewedit' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'reviewdelete' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'ruledelete' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'delete' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'ruleedit' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'rule' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'statiedit' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'stati' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'statidelete' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'additional' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'additionalrev' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'additionalstati' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        )
    );

    public function actionIndex() {
        $this->redirect('/cp/content/list');
    }

    public function actionCreate() {

        $content = new Content();
        if (isset($_POST['form'])) {



            $content->__attributes = $_POST['form'];
            $content->mod_time = time();
            if ($content->save()) {
                $this->loadImage('content/'.$content->id, 'preview.jpg');





                $article_page = Page::modelWhere('url = ?', array('stati'));
                if ($article_page) {
                    $article_page->mod_time = time();
                    $article_page->save();
                }
                $this->redirect('/cp/content/list/' . $content->id);
            }
        }
        $categoies = Category::modelsWhere('id ORDER BY id DESC');
        $this->render('index', array('categories' => $categoies, 'content' => $content));
    }

    private function loadImage($catalog = '', $fileName = '') {
        $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/uploads/';
        if(!empty($catalog)){
            $uploaddir .= $catalog . '/';
        }

        if(!file_exists($uploaddir)){
            mkdir($uploaddir, 0777, true);
        }

        $uploadfile = $uploaddir . basename($_FILES['userfile']['name']);
        
        if(!empty($fileName)){
            $uploadfile = $uploaddir . $fileName;
        }


        if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
            return $_FILES['userfile']['name'];
        } else {
            return false;
        }
    }

    public function actionView($id = 0) {
        $content;
        if ($id !== 0) {
            $content = Content::model((int) $id);
        }
        $this->render('view', array('content' => $content));
    }

    public function actionList() {
        $categories = Category::modelsWhere('id ORDER BY id DESC');

        if (count($categories)) {
            foreach ($categories as $category) {
                $category->articles = Content::modelsWhere('id_category = ? ORDER BY id DESC', array($category->id));
            }
        }
        $this->render('list', array('categories' => $categories));
    }

    public function actionEdit($id) {
        $content = Content::model((int) $id);
        if ($content) {
            if (isset($_POST['form'])) {
                $content->__attributes = $_POST['form'];
                $content->mod_time = time();
                if ($content->save()) {
                    $this->loadImage('content/'.$content->id, 'preview.jpg');
                    $article_page = Page::modelWhere('url = ?', array('stati'));
                    if ($article_page) {
                        $article_page->mod_time = time();
                        $article_page->save();
                    }
                    $this->redirect('/cp/content/list');
                }
            }
            $categories = Category::modelsWhere('id ORDER BY id DESC');
            $this->render('index', array('content' => $content, 'categories' => $categories));
        }
    }

    public function actionDelete($id) {
        if (Content::delete((int) $id)) {
            $article_page = Page::modelWhere('url = ?', array('stati'));
            if ($article_page) {
                $article_page->mod_time = time();
                $article_page->save();
            }
        }
        $this->redirect('/cp/content/list');
    }

    function actionFaq($id=0) {
        $page = PageVo::model((int)$id);
        if ($page) {            
            if (isset($_POST['form'])) {
                $faq = new Faq();
                $faq->__attributes = $_POST['form'];
                $faq->mod_time = time();
                if ($faq->save()) {
                    $this->refresh();
                }
            }
            $exists = Faq::modelsWhere('id_page = ? ORDER BY id DESC', array($page->id));
            $this->render('faq', array('page' => $page, 'exists'=>$exists));
        }else{
            $pages = PageVo::modelsWhere('id ORDER BY id DESC');
            $this->render('allfaq', array('pages'=>$pages));
        }        
    }
    
    function actionReview($id=0) {
        $page = PageReview::model((int)$id);
        if ($page) {            
            if (isset($_POST['form'])) {
                $review = new Review();
                $review->__attributes = $_POST['form'];
                if ($review->save()) {
                    $this->redirect('/cp/content/additionalrev/'.$review->id);
                }
            }
            $exists = Review::modelsWhere('id_page = ? ORDER BY id DESC', array($page->id));
            $this->render('review', array('page' => $page, 'exists'=>$exists));
        }else{
            $pages = PageReview::models();
            $this->render('allreview', array('pages'=>$pages));
        }         
    }

    public function actionFaqDelete($id, $page) {
        if(Faq::delete((int) $id)){
            $this->redirect('/cp/content/faq/'.$page);
        }
    }
    public function actionFaqEdit($id = 0) {
        $exists = array();
        $faq = Faq::model((int)$id);
        if($faq){
            if (isset($_POST['form'])) {
                $faq->__attributes = $_POST['form'];
                if ($faq->save()) {
                    $this->refresh();
                }
            }
            $exists = Faq::modelsWhere('id_page = ? ORDER BY id DESC', array($faq->id_page));
            $page = PageVo::model($faq->id_page);
            $this->render('editfaq', array('exists' => $exists, 'page'=>$page, 'faq'=>$faq));
        }else{
            $this->redirect('/cp');
        }
    }
    public function actionReviewDelete($id, $page) {
       if(Review::delete((int) $id)){
           $this->redirect('/cp/content/review/'.$page);
       }
    }
    public function actionRuleDelete($id, $page) {
       if(Rule::delete((int) $id)){
           $this->redirect('/cp/content/rule/'.$page);
       }
    }
    public function actionStatiDelete($id, $page) {
       if(Stati::delete((int) $id)){
           $this->redirect('/cp/content/stati/'.$page);
       }
    }
    
    public function actionReviewEdit($id = 0) {
        $exists = array();
        $review = Review::model((int)$id);
        if($review){
            if (isset($_POST['form'])) {
                $review->__attributes = $_POST['form'];
                if ($review->save()) {
                    $this->refresh();
                }
            }
            $exists = Review::modelsWhere('id_page = ? ORDER BY id DESC', array($review->id_page));
            $page = PageReview::model($review->id_page);
            $this->render('editreview', array('exists' => $exists, 'page'=>$page, 'review'=>$review));
        }else{
            $this->redirect('/cp');
        }
    }
    
    public function actionRuleEdit($id = 0) {
        $exists = array();
        $rule = Rule::model((int)$id);
        if($rule){
            if (isset($_POST['form'])) {
                $rule->__attributes = $_POST['form'];
                if ($rule->save()) {
                    $this->refresh();
                }
            }
            $exists = Rule::modelsWhere('id_page = ? ORDER BY id DESC', array($rule->id_page));
            $page = PageRule::model($rule->id_page);
            $this->render('editrule', array('exists' => $exists, 'page'=>$page, 'rule'=>$rule));
        }else{
            $this->redirect('/cp');
        }
    }
    
    function actionRule($id=0) {
        $page = PageRule::model((int)$id);
        if ($page) {            
            if (isset($_POST['form'])) {
                $rule = new Rule();
                $rule->__attributes = $_POST['form'];
                if ($rule->save()) {
                    $this->redirect('/cp/content/additional/'.$rule->id);
                }
            }
            $exists = Rule::modelsWhere('id_page = ? ORDER BY id DESC', array($page->id));
            $this->render('rule', array('page' => $page, 'exists'=>$exists));
        }else{
            $pages = PageRule::models();
            $this->render('allrules', array('pages'=>$pages));
        }         
    }
    function actionStati($id=0) {
        $page = PageStati::model((int)$id);
        if ($page) {            
            if (isset($_POST['form'])) {
                $stati = new Stati();
                $stati->__attributes = $_POST['form'];
                if ($stati->save()) {
                    $this->redirect('/cp/content/additionalstati/'.$stati->id);
                }
            }
            
            $exists = Stati::modelsWhere('id_page = ? ORDER BY id DESC', array($page->id));
            $this->render('stati', array('page'=>$page, 'exists'=>$exists));
        }else{
            $pages = PageStati::models();
            $this->render('allstati', array('pages'=>$pages));
        }         
    }
    function actionStatiEdit($id=0) {
        $exists = array();
        $stati = Stati::model((int)$id);
        if($stati){
            if (isset($_POST['form'])) {
                $stati->__attributes = $_POST['form'];
                if ($stati->save()) {
                    $this->refresh();
                }
            }
            $exists = Stati::modelsWhere('id_page = ? ORDER BY id DESC', array($stati->id_page));
            $page = PageStati::model($stati->id_page);
            $this->render('editstati', array('exists' => $exists, 'page'=>$page, 'stati'=>$stati));
        }else{
            $this->redirect('/cp');
        }
    }
    
    function actionAdditional($id=0) {
        $rule = Rule::model((int) $id);
        $exists = array();
        
        if ($rule) {
            if (isset($_FILES['userfile'])) {
                $img = $this->loadImage('rules');
                if ($img) {

                    $rule->img = $img;
                    if ($rule->save()) {
                        $this->refresh();
                    }
                }
            }
            $exists = Rule::models();
            $this->render('additional', array('rule'=>$rule, 'exists'=>$exists));
        }
    }
    
    function actionAdditionalRev($id=0) {
        $review = Review::model((int) $id);
        $exists = array();
        
        if ($review) {
            if (isset($_FILES['userfile'])) {
                $img = $this->loadImage('reviews');
                if ($img) {
                    $review->img = $img;
                    if ($review->save()) {
                        //$this->refresh();
                    }
                }
            }
            $exists = Review::models();
            $this->render('additionalrev', array('review'=>$review, 'exists'=>$exists));
        }
    }
    
    function actionAdditionalStati($id=0) {
        $stati = Stati::model((int) $id);
        $exists = array();
        
        if ($stati) {
            if (isset($_FILES['userfile'])) {
                $img = $this->loadImage('stati');
                if ($img) {
                    $stati->img = $img;
                    if ($stati->save()) {
                        //$this->refresh();
                    }
                }
            }
            $page = PageStati::model($stati->id_page);
            $exists = Stati::modelsWhere('id_page = ?', array($page->id));
            $this->render('additionalstati', array('stati'=>$stati, 'exists'=>$exists, 'page'=>$page));
        }
    }
    
    

}
