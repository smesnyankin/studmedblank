<?php

class SpravkiController extends Controller {

    static $rules = array(
        'index' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'create' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'edit' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'delete' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'additional' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'editadditional' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'deleteadditional' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
    );

    public function actionIndex() {
        $spravki = Spravka::models();
        $this->render('index', array('spravki' => $spravki));
    }

    public function actionCreate() {
        if (isset($_POST['form'])) {
            $spravka = new Spravka();
            $spravka->__attributes = $_POST['form'];
            if ($spravka->save()) {
                $this->redirect('/cp/spravki/additional/' . $spravka->id);
            }
        }
        $categories = Category::modelsWhere('id ORDER BY id DESC');
        $this->render('create', array('categories' => $categories));
    }

    public function actionAdditional($id = 0, $id_field = 0) {
        $spravka = Spravka::model((int) $id);
        $fields = array();
        $field = new Field();
        if ($spravka) {

            if (isset($_FILES['userfile'])) {
                $img = $this->loadImage();
                if ($img) {

                    $spravka->img = $img;
                    if ($spravka->save()) {
                        $this->refresh();
                    }
                }
            }
            $fields = Field::modelsWhere('id_spravka = ?', array($spravka->id));
            $field = new Field();
            if (Field::model((int) $id_field)) {
                $field = Field::model((int) $id_field);
            }
            $field->id_spravka = $spravka->id;
            if (isset($_POST['field'])) {
                $field->__attributes = $_POST['field'];
                if ($field->save()) {
                    $this->refresh();
                }
            }
        }
        $this->render('additional', array('spravka' => $spravka, 'fields' => $fields, 'field' => $field));
    }

    public function actionEditAdditional($id_spravka = 0, $id_field = 0) {
        $spravka = Spravka::model((int) $id_spravka);
        if ($spravka) {

        }
    }

    public function actionDeleteAdditional($id_spravka = 0, $id_field = 0) {
        $spravka = Spravka::model((int) $id_spravka);
        if ($spravka) {
            Field::delete((int) $id_field);
            $this->redirect('/cp/spravki/additional/' . $spravka->id);
        }
    }

    private function loadImage($catalog = '', $fileName = '') {
        $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/uploads/';
        if(!empty($catalog)){
            $uploaddir .= $catalog . '/';
        }

        if(!file_exists($uploaddir)){
            mkdir($uploaddir, 0777, true);
        }

        $uploadfile = $uploaddir . basename($_FILES['userfile']['name']);
        if(!empty($fileName)){
            $uploadfile = $fileName;
        }

        if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
            return $_FILES['userfile']['name'];
        } else {
            return false;
        }
    }

    public function actionDelete($id = 0) {
        if (Spravka::delete((int) $id)) {
            Field::deleteWhere('id_spravka = ?', array((int) $id));
        }
        $this->redirect('/cp/spravki/');
    }

    public function actionEdit($id_spravka = 0) {
        $spravka = Spravka::model((int) $id_spravka);
        if ($spravka) {
            if (isset($_POST['form'])) {
                $spravka->__attributes = $_POST['form'];
                if ($spravka->save()) {
                    $this->redirect('/cp/spravki/');
                }
            }
            $categories = Category::modelsWhere('id ORDER BY id DESC');
            $this->render('edit', array('spravka' => $spravka, 'categories' => $categories));
        } else {
            $this->redirect('/cp/spravki/');
        }
    }

}
