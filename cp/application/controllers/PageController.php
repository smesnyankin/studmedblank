<?php

class PageController extends Controller {

    static $rules = array(
        'index' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'main' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'about' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'article' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'catalog' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'pay' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'contactsanddelivery' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),        
        'faq' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'review' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'editfaq' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'editreview' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'editrule' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'rule' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'stati' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'editstati' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login')
    );

    function actionMain() {
        $page = Page::modelWhere('url = ?', array('index'));
        if (!$page) {
            $page = new Page();
            $page->url = 'index';
            if (!$page->save()) {
                $this->redirect('/cp/error/404');
            }
        }

        if (isset($_POST['form'])) {
            $page->__attributes = $_POST['form'];
            $page->mod_time = time();
            if ($page->save()) {
                $this->refresh();
            }
        }

        $this->render('main', array('page' => $page));
    }

    function actionAbout() {
        $page = Page::modelWhere('url = ?', array('o-kompanii'));
        if (!$page) {
            $page = new Page();
            $page->url = 'o-kompanii';
            if (!$page->save()) {
                $this->redirect('/o-kompanii');
            }
        }
        if (isset($_POST['form'])) {
            $page->__attributes = $_POST['form'];
            $page->mod_time = time();
            if ($page->save()) {
                $this->refresh();
            }
        }
        $this->render('about', array('page' => $page));
    }

    function actionArticle() {
        $page = Page::modelWhere('url = ?', array('stati'));
        if (!$page) {
            $page = new Page();
            $page->url = 'stati';
            if (!$page->save()) {
                $this->redirect('/cp/error/404');
            }
        }
        if (isset($_POST['form'])) {
            $page->__attributes = $_POST['form'];
            $page->mod_time = time();
            if ($page->save()) {
                $this->refresh();
            }
        }
        $this->render('article', array('page' => $page));
    }

    function actionCatalog() {
        $page = Page::modelWhere('url = ?', array('katalog'));
        if (!$page) {
            $page = new Page();
            $page->url = 'katalog';
            if (!$page->save()) {
                $this->redirect('/cp/error/404');
            }
        }
        if (isset($_POST['form'])) {
            $page->__attributes = $_POST['form'];
            $page->mod_time = time();
            if ($page->save()) {
                $this->refresh();
            }
        }
        $this->render('catalog', array('page' => $page));
    }

    function actionPay() {
        $page = Page::modelWhere('url = ?', array('oplata'));
        if (!$page) {
            $page = new Page();
            $page->url = 'oplata';
            if (!$page->save()) {
                $this->redirect('/cp/error/404');
            }
        }
        if (isset($_POST['form'])) {
            $page->__attributes = $_POST['form'];
            $page->mod_time = time();
            if ($page->save()) {
                $this->refresh();
            }
        }
        $this->render('pay', array('page' => $page));
    }
    function actionContactsAndDelivery() {
        $page = Page::modelWhere('url = ?', array('dostavka-i-kontakty'));
        if (!$page) {
            $page = new Page();
            $page->url = 'dostavka-i-kontakty';
            if (!$page->save()) {
                $this->redirect('/cp/error/404');
            }
        }
        if (isset($_POST['form'])) {
            $page->__attributes = $_POST['form'];
            $page->mod_time = time();
            if ($page->save()) {
                $this->refresh();
            }
        }
        $this->render('contactsanddelivery', array('page' => $page));
    }

    function actionFaq() {        
        $exists = array();          
        if (isset($_POST['form'])) {
            $page = new PageVo();
            $page->__attributes = $_POST['form'];
            $page->mod_time = time();
            if ($page->save()) {
                $this->refresh();
            }
        }
        $exists = PageVo::modelsWhere('id ORDER BY id DESC');
        $this->render('faq', array('exists' => $exists));
        
    }
    
    function actionReview() {   
        $exists = array();          
        if (isset($_POST['form'])) {
            $review = new PageReview();
            $review->__attributes = $_POST['form'];
            if ($review->save()) {
                $this->refresh();
            }
        }
        $exists = PageReview::modelsWhere('id ORDER BY id DESC');
        $this->render('review', array('exists'=>$exists));
    }
    
    function actionStati() {   
        $exists = array();          
        if (isset($_POST['form'])) {
            $stati = new PageStati();
            $stati->__attributes = $_POST['form'];
            if ($stati->save()) {
                $this->refresh();
            }
        }
        $exists = PageStati::models('ORDER BY id DESC');
        $this->render('stati', array('exists'=>$exists));
    }
    
    function actionEditStati($id=0) {   
        $exists = array();
        $page = PageStati::model((int)$id);
        if($page){
            if (isset($_POST['form'])) {
                $page->__attributes = $_POST['form'];
                if ($page->save()) {
                    $this->refresh();
                }
            }
            $exists = PageStati::models('ORDER BY id DESC');
            $this->render('editstati', array('exists'=>$exists, 'page'=>$page));
        }else{
            $this->redirect('/cp');
        }
    } 
    
    function actionEditFaq($id=0) {        
        $exists = array();
        $page = PageVo::model((int)$id);
        if($page){
            if (isset($_POST['form'])) {
                $page->__attributes = $_POST['form'];
                $page->mod_time = time();
                if ($page->save()) {
                    $this->refresh();
                }
            }
            $exists = PageVo::models('ORDER BY id DESC');
            $this->render('editfaq', array('exists' => $exists, 'page'=>$page));
        }else{
            $this->redirect('/cp');
        }
        
    }
    
    function actionEditReview($id=0) {   
        $exists = array();
        $page = PageReview::model((int)$id);
        if($page){
            if (isset($_POST['form'])) {
                $page->__attributes = $_POST['form'];
                if ($page->save()) {
                    $this->refresh();
                }
            }
            $exists = PageReview::models('ORDER BY id DESC');
            $this->render('editreview', array('exists'=>$exists, 'page'=>$page));
        }else{
            $this->redirect('/cp');
        }
    }        
    function actionEditRule($id=0) {   
        $exists = array();
        $page = PageRule::model((int)$id);
        if($page){
            if (isset($_POST['form'])) {
                $page->__attributes = $_POST['form'];
                if ($page->save()) {
                    $this->refresh();
                }
            }
            $exists = PageRule::models('ORDER BY id DESC');
            $this->render('editrule', array('exists'=>$exists, 'page'=>$page));
        }else{
            $this->redirect('/cp');
        }
    }        
    function actionRule() {   
        $exists = array();          
        if (isset($_POST['form'])) {
            $rule = new PageRule();
            $rule->__attributes = $_POST['form'];
            if ($rule->save()) {
                $this->refresh();
            }
        }
        $exists = PageRule::models('ORDER BY id DESC');
        $this->render('rule', array('exists'=>$exists));
    }       
}

