<?php
class BlogController extends Controller {
    static $rules = array(
            'index' => array(
                    'users' => array('admin'),
                    'redirect' => '/cp/login'),		
            'add' => array(
                    'users' => array('admin'),
                    'redirect' => '/cp/login'),		
            'delete' => array(
                    'users' => array('admin'),
                    'redirect' => '/cp/login'),
            'edit' => array(
                    'users' => array('admin'),
                    'redirect' => '/cp/login'),
            'additional' => array(
                    'users' => array('admin'),
                    'redirect' => '/cp/login'),
            'config' => array(
                    'users' => array('admin'),
                    'redirect' => '/cp/login')
            );

    public function actionIndex(){
        $exists = AutoPost::modelsWhere('id ORDER BY id DESC');
        $this->render('index', array('exists'=>$exists));
    }

    public function actionAdd(){
        $exists = array();
        if (isset($_POST['form'])) {
            $post = new AutoPost();
            $post->__attributes = $_POST['form'];
            if ($post->save()) {
                $this->redirect('/cp/blog/additional/'.$post->id);
            }
        }
        $exists = AutoPost::modelsWhere('id ORDER BY id DESC');
        $this->render('add', array('exists'=>$exists));
    }
    
    public function actionAdditional($id = 0) {
        $post = AutoPost::model((int) $id);
        if ($post) {            
            if (isset($_FILES['userfile'])) {
                $img = $this->loadImage();
                if($img){
                    $post->img = $img;
                    $post->save();
                    $this->refresh();
                }
            }            
        }
        $exists = AutoPost::modelsWhere('id ORDER BY id DESC');
        $this->render('additional', array('post' => $post, 'exists'=>$exists));
    }
    
    private function loadImage($catalog = '') {
        $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/uploads/blog/';
        if(!empty($catalog)){
            $uploaddir .= $catalog . '/';
        }
        if(!file_exists($uploaddir)){
            mkdir($uploaddir,0777,true);
        }
        $uploadfile = $uploaddir . basename($_FILES['userfile']['name']);
                
        if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
            return $_FILES['userfile']['name'];
        } else {
            return false;
        }
    }
    
    public function actionEdit($id=0){
        $exists = array();
        $post=null;
        $post = AutoPost::model((int)$id);
        if($post){
            if (isset($_POST['form'])) {
                $post->__attributes = $_POST['form'];
                if ($post->save()) {
                    $this->refresh();
                }
            }
            $exists = AutoPost::modelsWhere('id ORDER BY id DESC');
            $this->render('edit', array('post'=>$post,'exists'=>$exists));
        }else{
            $this->redirect('/cp');
        }
    }
    
    public function actionConfig(){
        $config = PostConfig::modelWhere('id');
        if (isset($_POST['form'])) {
            $config->intervall = (int)$_POST['form']['hour']*3600+(int)$_POST['form']['day']*24*3600;
            $config->post_count = $_POST['form']['post_count'];
            $config->save();
        }
        $this->render('config', array('config'=>$config));

    }
    public function actionDelete($id = 0){
        AutoPost::delete((int) $id);
        $this->redirect('/cp/blog');

    }

}