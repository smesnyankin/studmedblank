<?php

class IndexController extends Controller {

    static $rules = array(
        'index' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'login' => array(
            'users' => array('guest'),
        )
    );

    public function actionIndex() {
        $this->render('index');
    }
}

