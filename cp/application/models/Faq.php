<?php
class Faq extends ModelTable{
	static $table = 'faq';
	public $safe = array('id', 'id_page', 'answer', 'question');
}