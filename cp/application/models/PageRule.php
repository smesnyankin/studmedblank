<?php 
class PageRule extends ModelTable {
	static $table = 'page_rule';
	public $safe = array('id', 'title', 'text', 'meta_title', 'meta_keywords', 'meta_description');
}