<?php
class Spravka extends ModelTable {
	static $table = 'spravki';
	public $safe = array('id', 'id_category', 'price', 'title', 'url', 'img', 'content', 'meta_title', 'meta_keywords', 'meta_description');

	public $fields = array();
}