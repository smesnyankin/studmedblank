<?php 
class PageVo extends ModelTable {
	static $table = 'page_vo';
	public $safe = array('id', 'url', 'title', 'meta_title', 'meta_keywords', 'meta_description', 'content', 'mod_time');
}