<?php 
class PageStati extends ModelTable {
	static $table = 'page_stati';
	public $safe = array('id', 'url', 'title', 'meta_title', 'meta_keywords', 'meta_description');
        
        public static function getName($id=0){
            return self::model((int)$id)->page_name;
        }
}