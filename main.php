<?php
define('ROOT', dirname(__FILE__) . '/');
define('CORE', dirname(__FILE__) . '/core/'); //каталог ядра
define('APP', dirname(__FILE__) . '/application/'); //каталог фронтальной части
//define('CP', dirname(__FILE__) . '/cp/'); //каталог панели управления

require_once CORE . 'autoload.php';
require_once CORE . 'functions.php';

App::gi()->start();