<?php

class SendController extends Controller {

    function actionIndex() {
       if(isset($_POST['fio'])){
            $message = '';
            $msg = '';
            $titles = array();
            $urls = array();
            $allsprvk = Spravka::models();
            
            foreach($allsprvk as $sprvk){
                    $form = Field::modelsWhere('id_spravka = ?', array($sprvk->id));
                    foreach($form as $field){
                        if(!in_array($field->url,$urls)){
                            $titles[] = $field->title; 
                            $urls[] = $field->url; 
                        }
                    }
            }
            
            $to  = 'spravka.com@yandex.ru, apmagedon2@gmail.com';
//            $email_admin= 'info@spravki-v-moskve.com'; 
            $codepage = 'utf8';
            $subject = 'Запрос на покупку справки на spravki-v-moskve';
            
//            if(isset($_POST['data_rojdeniya'])){
//                $dr = explode('-',$_POST['data_rojdeniya']);
//                if((int)$dr[0]<1900){
//                    $this->redirect($_SERVER['HTTP_REFERER']);
//                }
//            }
            
            $c = count($titles);
            for($i=0;$i<$c;$i++){
                if(isset($_POST["$urls[$i]"])){
                    $msg.= "$titles[$i] ".$_POST["$urls[$i]"]."\r\n";
                }
            }
            unset($c);
            
            if(isset($_POST['metro'])){
                 $msg.= "Нужна ли доставка до станции метро: ".$_POST["metro"]."\r\n";
            }
            if(isset($_POST['comments'])){
                 $msg.= "Комментарий: ".$_POST["comments"]."\r\n";
            }
            
            $headers = "Content-Type: text/html; charset=$codepage\r\n";
//            $headers .= "From: $email_admin" . "\r\n";
            $headers .= "X-Mailer: My Send E-mail\r\n"; 
            
            if(mail($to, $subject, $msg)){
                $message = "Спасибо, Ваша заявка принята. Мы свяжемся с Вами в ближайшее время. Через 5 секунд вы будете перенапрвлены на страницу с которой пришли.";	
            } else {
                $message = "Не удалось отправить сообщение. Через 5 секунд вы будете перенапрвлены на страницу с которой пришли.";
            }

            $this->render('ok', array('message'=>$message,'msg'=>$msg));
        }
    }

    public function actionMainDown() {
        if(isset($_POST['name'])){
            $message = '';
            $msg = '';
            
            $to  = 'spravka.com@yandex.ru, apmagedon2@gmail.com';
//            $email_admin= 'info@spravki-v-moskve.com'; 
            $codepage = 'utf8';
            $subject = 'Запрос на покупку справки на spravki-v-moskve';
            
                $msg .= "Имя: ${_POST['name']}\r\n";
                $msg .= "Номер телефона: ${_POST['phone']}\r\n";
                $msg .= "Город: ${_POST['town']}\r\n";
                $msg .= "Коментрарии: ${_POST['comment']}\r\n";
            
            $headers = "Content-Type: text/html; charset=$codepage\r\n";
//            $headers .= "From: $email_admin" . "\r\n";
//            $headers .= "X-Mailer: My Send E-mail\r\n"; 
            
            if(mail($to, $subject, $msg)){
                $message = "Спасибо, Ваша заявка принята. Мы свяжемся с Вами в ближайшее время. Через 5 секунд вы будете перенапрвлены на страницу с которой пришли.";	
            } else {
                $message = "Не удалось отправить сообщение. Через 5 секунд вы будете перенапрвлены на страницу с которой пришли.";
            }

            $this->render('ok', array('message'=>$message,'msg'=>$msg));
        }
    }
    public function actionMain() {
        if(isset($_POST['name'])){
            $message = '';
            $msg = '';
            
            $to  = 'spravka.com@yandex.ru, apmagedon2@gmail.com';
//            $email_admin= 'info@spravki-v-moskve.com'; 
            $codepage = 'utf8';
            $subject = 'Запрос на заказ справки на spravki-v-moskve';
            
                $msg .= "Имя: ${_POST['name']}\r\n";
                $msg .= "Адрес прописки: ${_POST['adres_propiski']}\r\n";
                $msg .= "Номер телефона: ${_POST['phone']}\r\n";
                $msg .= "Сроки заболевания: ${_POST['sroki']}\r\n";
                $msg .= "Нужна ли доставка к метро: ${_POST['dostavka']}\r\n";
                $msg .= "Коментрарии: ${_POST['comment']}\r\n";
            
            $headers = "Content-Type: text/html; charset=$codepage\r\n";
//            $headers .= "From: $email_admin" . "\r\n";
            $headers .= "X-Mailer: My Send E-mail\r\n"; 
            
            if(mail($to, $subject, $msg)){
                $message = "Спасибо, Ваша заявка принята. Мы свяжемся с Вами в ближайшее время. Через 5 секунд вы будете перенапрвлены на страницу с которой пришли.";	
            } else {
                $message = "Не удалось отправить сообщение. Через 5 секунд вы будете перенапрвлены на страницу с которой пришли.";
            }

            $this->render('ok', array('message'=>$message,'msg'=>$msg));
        }
    }
}
