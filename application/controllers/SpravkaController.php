<?php
class SpravkaController extends Controller {
    public function actionView($url = ''){
        if(!empty($url)){
            $spravka = Spravka::modelWhere('url = ?', array($url));
            if($spravka){
                $fields = Field::modelsWhere('id_spravka = ?', array($spravka->id));
                
                $this->setBreadCrumbs('/', 'Главная');
                $this->setBreadCrumbs('#', 'Справка'.$spravka->title);
            
                $this->render('view', array('spravka' => $spravka, 'fields' => $fields));
                return;
            }
        }
        $this->redirect('/');
    }
    
    public function actionSearch(){
        if(isset($_POST['spravka_name']) and !empty($_POST['spravka_name'])){
            $page = Page::modelWhere('url = ?', array('find'));

            if ($page) {
                $meta_title = $page->meta_title;
                if (!empty($meta_title)) {
                    $this->meta_title = $meta_title;
                }
                $this->meta_keywords = $page->meta_keywords;
                $this->meta_description = $page->meta_description;
                $this->lastModified($page->mod_time);

            }
            
            $query = trim($_POST['spravka_name']);
            $search = mb_convert_case ( $query , MB_CASE_LOWER ,'utf-8');
            $categories = Category::modelsWhere('id ORDER BY id ASC');
            if($categories){
                foreach ($categories as $category){
                    $category->articles = Spravka::modelsWhere('id_category = ? AND LOWER(title) LIKE ?', array($category->id,'%'.$search.'%'));;
               }
            }
            if(Spravka::modelsWhere('title LIKE ? ORDER BY id ASC', array('%'.$search.'%'))){
                $message = 'Результаты поиска по запросу: '.$query;
            }else{
                $message = 'По запросу "'.$query.'" ничего не найдено, проверте правильность написания вашего запроса,'
                        . ' возможно вы допустили ошибку.';
            }
            $this->render('find', array('page' => $page, 'categories' => $categories, 'message'=>$message));
        }else{
            $this->redirect("/");
        }
    }
    
    public function actionList()
    {
		$spravki = Spravka::models();
		echo '<h2>Список справок и их url</h2>';
		echo '<table border="1" style="font-size: 16px; width:100%;">';
		echo '<tr><td>Название</td><td>Url</td></tr>';
		foreach($spravki as $spravka){
			echo '<tr style="border-bottom: solid 1px black;"><td>'. $spravka->title .'</td><td>spravka/' . $spravka->url . '</td></tr>';
		}
		echo '</table>';
	}
}

