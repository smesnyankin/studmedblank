<?php

class BlogController extends Controller {
    public function actionIndex(){
        
    }
    
    function actionPage($id=0) {
        $this->getNewPost();
        if($id !=0){
            $page = Page::model($id);
        }else{
            $page = Page::modelWhere('url = ?', array('blog'));
        }
        
        $list = BlogPost::modelsWhere('id_page = ?', array($page->id));

        $this->meta_title = 'Блог нашего сайта';
        $this->meta_keywords = '';
        $this->meta_description = '';

//        $this->setBreadCrumbs('/', 'Главная');
//        $this->setBreadCrumbs('#', 'Блог');

        $pages = Page::modelsWhere('url LIKE ?', array('%blog%'));
        
        $this->setBreadCrumbs('/', 'Главная');
        $this->setBreadCrumbs('#', 'Блог');
         
        $this->render('blog', array('list'=>$list, 'pages'=>$pages));
    }
    
    function actionPost($url) {
        $blogarticle = BlogPost::modelWhere('go_url = ?', array($url));
        if($blogarticle){
            $this->meta_title = $blogarticle->meta_title;
            $this->meta_keywords = $blogarticle->meta_keywords;
            $this->meta_description = $blogarticle->meta_description;
//
            $this->setBreadCrumbs('/', 'Главная');
            $this->setBreadCrumbs('/blog', 'Блог');
            $this->setBreadCrumbs('#', $blogarticle->title);

            $this->render('blogarticle', array('post'=>$blogarticle));
        }
    }
    
    public function  getNewPost($id=0){ 
        $time = time();		
        $config = PostConfig::modelWhere('id');

        if(($config->last_time+$config->intervall)<$time){
//            if($config->post_count > 1){
//                $this->multiplePost($config,$time);
//            }else{
                $this->singlePost($config,$time);
//            }
        }
    }
	
//    public function multiplePost(ModelTable $config,$time){
//        $autopost = AutoPost::modelsWhere('id ORDER BY id LIMIT ?', array($config->post_count));
//        if(count($autopost)==$config->post_count){
//            foreach($autopost as $post){
//            //Привязка к странице
//                $lastpost = BlogPost::modelWhere('id ORDER BY id DESC');
//                if($lastpost){					
//                    //Проверка на заполненность страници к которой привязан предидущий пост
//                    $page = $this->checkPage($lastpost->id_page);				
//                    //----------------------------------------------------------------------
//                }else{
//                    //Проверка на существование не до конца заполненой страницы
//                    $check = Cpravka::modelWhere('page_type = ? ORDER BY id DESC', array('blog'));
//                    $page = $this->checkPage($check->id, $config->post_count);
//                    //---------------------------------------------------------
//                }
//                $exist = new BlogPost();
//                $exist->id_page = $page->id;
//                $exist->go_url = $page->url.'/'.$post->go_url;
//                $exist->title=$post->title;
//                $exist->text=$post->text;
//                $exist->img=$post->img;
//                $exist->meta_title=$post->meta_title;
//                $exist->meta_keywords=$post->meta_keywords;
//                $exist->meta_description=$post->meta_description;
//                $exist->time=$time;
//                if($exist->save()){
//                    AutoPost::delete($post->id);
//                }                
//            //-------------------		
//            }
//            $config->last_time = $time;
//            $config->save();
//        }
//    }
    
    public function singlePost(ModelTable $config,$time){         
        $autopost = AutoPost::modelWhere('id ORDER BY id');
        if($autopost){
            //Привязка к странице
                $lastpost = BlogPost::modelWhere('id ORDER BY id DESC');
                if($lastpost){					
                    //Проверка на заполненность страници к которой привязан предидущий пост
                    $page = $this->checkPage($lastpost->id_page);
                    //----------------------------------------------------------------------
                }else{
                    //Проверка на существование не до конца заполненой страницы
                    $check = Page::modelWhere('url LIKE ? ORDER BY id DESC', array('%blog%'));
                    if($check){
                        $page = $this->checkPage($check->id);
                    }else{
                        $page = $this->createPage();
                    }
                    //---------------------------------------------------------
                }
                $exist = new BlogPost();
                $exist->id_page = $page->id;
                $exist->go_url = $autopost->go_url;
                $exist->title=$autopost->title;
                $exist->text=$autopost->text;
                $exist->img=$autopost->img;
                $exist->meta_title=$autopost->meta_title;
                $exist->meta_keywords=$autopost->meta_keywords;
                $exist->meta_description=$autopost->meta_description;
                $exist->time=$time;
                if($exist->save()){
                    AutoPost::delete($autopost->id);
                }                
            //-------------------
            $config->last_time = $time;
            $config->save();
            
        }
    }
    
    public function checkPage($id_page, $postcount=0){
        $count = BlogPost::countRowWhere('id_page = ?', array($id_page));
        if($count > 7){ //Если добавляется больше одного поста???
            $page = $this->createPage();
        }else{
            $page = Page::model($id_page);
        }
        return $page;
    }
    
    public function createPage(){
        $page = new Page();
        $endurl = Page::countRowWhere('url LIKE ?', array('%blog%'));
        $page->url = 'blog'.(int)$endurl;
        if($page->save()){
            return $page;
        }else{
            return false;
        }
    }
    
}
