<?php

class ErrorController extends Controller {

    function actionIndex() {
        $this->redirect('/error/404');
    }

    function action404() {
        $this->meta_title = '404';
        header("HTTP/1.x 404 Not Found");
        $this->render('404');
    }
}
