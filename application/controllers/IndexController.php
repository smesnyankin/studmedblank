<?php

class IndexController extends Controller {

    function actionIndex($url = '') {
        $page = Page::modelWhere('url = ?', array('index'));

        if ($page) {
            $meta_title = $page->meta_title;
            if (!empty($meta_title)) {
                $this->meta_title = $meta_title;
            }
            $this->meta_keywords = $page->meta_keywords;
            $this->meta_description = $page->meta_description;
            $this->lastModified($page->mod_time);
        }

        $categories = Category::modelsWhere('id ORDER BY id ASC');
        if ($categories) {
            foreach ($categories as $category) {
                $category->articles = Spravka::modelsWhere('id_category = ? ORDER BY id ASC', array($category->id));
            }
        }

        $reviews = Review::modelsWhere('id LIMIT 9');

        $this->render('index', array('page' => $page, 'categories' => $categories, 'reviews' => $reviews));
    }

//    public function actionAbout() {
//        $page = Page::modelWhere('url = ?', array('o-kompanii'));
//        if ($page) {
//            $meta_title = $page->meta_title;
//            if (!empty($meta_title)) {
//                $this->meta_title = $meta_title;
//            }
//            $this->meta_keywords = $page->meta_keywords;
//            $this->meta_description = $page->meta_description;
//            $this->lastModified($page->mod_time);
//        }
//
//        $this->render('about', array('page' => $page));
//    }

//   public function actionArticles($id=0) {
//       var_dump($id);
//       if((int)$id){
//           echo 1;
//          $page = PageStati::modelWhere((int)$id);
//       }else{
//           echo 2;
//          $page = PageStati::modelWhere('id');           
//       }
//           
//      if ($page) {
//         $meta_title = $page->meta_title;
//         if (!empty($meta_title)) {
//            $this->meta_title = $meta_title;
//         }
//         $this->meta_keywords = $page->meta_keywords;
//         $this->meta_description = $page->meta_description;
//      }
//      $articles = Stati::modelsWhere('id_page = ? ORDER BY id DESC', array($page->id));
//      $pages = PageStati::models();
//      $this->render('articles', array('page' => $page, 'articles' => $articles, 'pages'=>$pages));
//   }

    function actionPay() {
        $page = Page::modelWhere('url = ?', array('oplata'));
        if ($page) {
            $meta_title = $page->meta_title;
            if (!empty($meta_title)) {
                $this->meta_title = $meta_title;
            }
            $this->meta_keywords = $page->meta_keywords;
            $this->meta_description = $page->meta_description;
            $this->lastModified($page->mod_time);
        }
        $this->render('pay', array('page' => $page));
    }

    function actionCatalog() {
        $categories = Category::modelsWhere('id ORDER BY id ASC');
        if ($categories) {
            foreach ($categories as $category) {
                $category->articles = Spravka::modelsWhere('id_category = ? ORDER BY id ASC', array($category->id));
            }
        }

        $this->setBreadCrumbs('/', 'Главная');
        $this->setBreadCrumbs('#', 'Каталог справок');

        $this->render('catalog', array('categories' => $categories));
    }

    function actionContactsAndDelivery() {
        $page = Page::modelWhere('url = ?', array('dostavka-i-kontakty'));
        if ($page) {
            $meta_title = $page->meta_title;
            if (!empty($meta_title)) {
                $this->meta_title = $meta_title;
            }
            $this->meta_keywords = $page->meta_keywords;
            $this->meta_description = $page->meta_description;
            $this->lastModified($page->mod_time);

            $this->setBreadCrumbs('/', 'Главная');
            $this->setBreadCrumbs('#', 'Контакты и доставка');

            $this->render('delivery', array('page' => $page));
        }
    }
//
//    function actionRead($url = '') {
//        if (!empty($url)) {
//            $article = Stati::modelWhere('go_url = ?', array($url));
//            if ($article) {
//                $meta_title = $article->meta_title;
//                if (!empty($meta_title)) {
//                    $this->meta_title = $meta_title;
//                }
//                $this->meta_keywords = $article->meta_keywords;
//                $this->meta_description = $article->meta_description;
//                $this->render('read', array('article' => $article));
//                return;
//            }
//        }
//
//        $this->redirect('/');
//    }

//
//   function actionFaq() {
//      $page = Page::modelWhere('url = ?', array('vopros-otvet'));
//      if ($page) {
//         $meta_title = $page->meta_title;
//         if (!empty($meta_title)) {
//            $this->meta_title = $meta_title;
//         }
//         $this->meta_keywords = $page->meta_keywords;
//         $this->meta_description = $page->meta_description;
//         $this->lastModified($page->mod_time);
//      }
//      $faqs = Faq::modelsWhere('id ORDER BY id DESC');
//      $this->render('faq', array('page' => $page, 'faqs' => $faqs));
//   }

    public function actionFaq($id = 0) {
        if ((int) $id != 0) {
            $page = PageVo::model((int) $id);
        } else {
            $page = PageVo::modelWhere('id');
        }
        if ($page) {
            $meta_title = $page->meta_title;
            if (!empty($meta_title)) {
                $this->meta_title = $meta_title;
            }
            $this->meta_keywords = $page->meta_keywords;
            $this->meta_description = $page->meta_description;
            $this->lastModified($page->mod_time);
        }

        $faqs = Faq::modelsWhere('id_page = ? ', array($page->id));
        $pages = PageVo::models();

        $this->setBreadCrumbs('/', 'Главная');
        $this->setBreadCrumbs('#', 'Вопросы и ответы');

        $this->render('faq', array('page' => $page, 'faqs' => $faqs, 'pages' => $pages));
    }

    function actionMail() {
        $this->render('mail');
    }

    private function lastModified($time) {
        if (!empty($time)) {
            $last_modidied_time = strtotime(date("D, d M Y H:i:s", $time));
            $last_modidied = gmdate("D, d M Y H:i:s \G\M\T", $last_modidied_time);
            header('Last-Modified: ' . $last_modidied);
        }
    }

}
