<?php

class PageController extends Controller {

    public function actionRules($id = 0) {
        if ((int) $id != 0) {
            $page = PageRule::model((int) $id);
        } else {
            $page = PageRule::model(1);
        }

        if ($page) {
            $meta_title = $page->meta_title;
            if (!empty($meta_title)) {
                $this->meta_title = $meta_title;
            }
            $this->meta_keywords = $page->meta_keywords;
            $this->meta_description = $page->meta_description;

            $rules = Rule::modelsWhere('id_page = ? ORDER BY id DESC', array($page->id));
            $pages = PageRule::models();

            $this->setBreadCrumbs('/', 'Главная');
            $this->setBreadCrumbs('#', 'Правила заполнения');

            $this->render('rule', array('page' => $page, 'rules' => $rules, 'pages' => $pages));
        }
    }

    public function actionReview($id = 0) {
        if ((int) $id != 0) {
            $page = PageReview::model((int) $id);
        } else {
            $page = PageReview::modelWhere('id');
        }
        if ($page) {
            $meta_title = $page->meta_title;
            if (!empty($meta_title)) {
                $this->meta_title = $meta_title;
            }
            $this->meta_keywords = $page->meta_keywords;
            $this->meta_description = $page->meta_description;

            $reviews = Review::modelsWhere('id_page = ? ORDER BY id DESC', array($page->id));
            $pages = PageReview::models();

            $this->setBreadCrumbs('/', 'Главная');
            $this->setBreadCrumbs('#', 'Отзывы');

            $this->render('review', array('page' => $page, 'reviews' => $reviews, 'pages' => $pages));
        }
//      $this->render('review');
    }

    public function actionArticles($id = 0) {
        if ((int) $id != 0) {
            $page = PageStati::model((int) $id);
        } else {
            $page = PageStati::modelWhere('id');
        }

        if ($page) {
            $meta_title = $page->meta_title;
            if (!empty($meta_title)) {
                $this->meta_title = $meta_title;
            }
            $this->meta_keywords = $page->meta_keywords;
            $this->meta_description = $page->meta_description;
        }
        $articles = Stati::modelsWhere('id_page = ? ORDER BY id DESC', array($page->id));
        $pages = PageStati::models();

        $this->setBreadCrumbs('/', 'Главная');
        $this->setBreadCrumbs('#', 'Статьи');

        $this->render('articles', array('page' => $page, 'articles' => $articles, 'pages' => $pages));
    }

//    public function actionFaq($id=0) {
//       if((int)$id !=0){
//          $page = PageVo::model((int)$id);
//       }else{
//          $page = PageVo::modelWhere('id');
//       }
//      if ($page) {
//         $meta_title = $page->meta_title;
//         if (!empty($meta_title)) {
//            $this->meta_title = $meta_title;
//         }
//         $this->meta_keywords = $page->meta_keywords;
//         $this->meta_description = $page->meta_description;
//         $this->lastModified($page->mod_time);
//      }
//      
//      $faqs = Faq::modelsWhere('id_page = ? ORDER BY id DESC', array($page->id));
//      $pages = PageVo::models();
//      $this->render('faq', array('page' => $page, 'faqs' => $faqs, 'pages'=>$pages));
//   }
}
