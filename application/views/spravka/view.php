<div id="preliminary-inspection">
	<div class="pre-inspection">
		
		<h5>Оформить <?= $spravka->title; ?> за <?= $spravka->price; ?> рублей</h5>
		<div class="img-sertificate">
			<a class="lightbox" href="#fancybox2">
				<img src="/uploads/<?= $spravka->img; ?>">
			</a>
			<div class="fancybox-holder">
				<div id="fancybox2" class="lightbox">
					<img src="/uploads/<?= $spravka->img; ?>">
				</div>
			</div>
		</div>
		<div class="right-section">
			<div class="price-box">Цена <?= $spravka->price; ?> руб.</div>
			<div class="forms-block">
				<h5>Оформление справки</h5>
				<div class="forms-set">
					<form method="POST" action="/send" class="validate">
						<fieldset class="data">
							<?php if ($fields): ?>
                                <?php foreach ($fields as $field): 
                                    if($field->url !=='data_rozhdeniya' and $field->url !=='dostavka-k-metro'){ ?>
                                        <label><input class="required" type="text" name="<?= $field->url; ?>" placeholder="<?= $field->title; ?>"></label><br>
                                    <?php }elseif($field->url !=='dostavka-k-metro'){ ?>
                                        <label><input id="datepicker" type="text" name="<?=$field->url; ?>" class="datepicker required" placeholder="<?= $field->title; ?>"></label><br>
                                    <?php }else{ ?>
                                        <strong><?=$field->title; ?><br></strong>
                                            <select name="<?=$field->url; ?>">
                                                <option value="Да">Да</option>
                                                <option value="Нет">Нет</option>
                                            </select>
                                    <?php } ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
						</fieldset>
						<fieldset class="text-btn">
							<label><textarea name="form[comments]" placeholder="Коментарии"></textarea></label>
							<input type="submit" value="Оформить заказ">
						</fieldset>
					</form>
					<span>Все поля обязательны для заполнения*</span>
				</div>
			</div>
		</div>
		<div class="inform-block">
			<?= $spravka->content; ?>
		</div>
	</div>
	<span>Для увеличения справки кликините по изображению!</span>
</div>
<!--
<div class="c_left">
		<p class="head-title">Название справки</p>
		<div class="document">
		    <div class="document-image">
		        <img src="images/001_s.jpg" alt="">
		    </div>
		    <div class="document-form">
		        <form action="">
                    <div class="form-info">
                        <input type="text">
                        <input type="text">
                        <input type="text">
                    </div>
                    <div class="form-holder">
                        <textarea>

                        </textarea>
                        <input type="submit" value="Отправить" class="btn btn-3 btn-3e ">
                    </div>
                </form>
		    </div>
		    <p class="document-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque dolor, ipsam, aut suscipit hic est corporis eveniet magni quam quas dolorum aliquid perspiciatis consequatur voluptas sequi qui sint dignissimos quaerat rem adipisci. Perferendis dolores delectus provident ipsum numquam nulla, saepe deleniti nam. Ipsa, eos ducimus porro obcaecati, ratione delectus alias. Atque voluptas eos culpa repudiandae quaerat incidunt animi fugiat nisi, magni necessitatibus dolorum eius voluptate nesciunt sequi dolore ducimus rem. Ad eligendi alias quia neque reprehenderit accusamus adipisci maiores explicabo eius sed sunt autem incidunt, sapiente repudiandae iusto voluptatum, omnis vel similique numquam. Rem doloribus tempore, repellat quidem debitis error.</p>
		</div>
		<div class="clear"></div>
	    </div>
	    <div class="c_right">
				<div class="moduletable">
                    <p class="head-title">Как мы работаем</p>

                    <ul class="grid_bg latestnews">
                        <li class="grid">
                            <div class="grid_img">
                                <img src="images/news_noimage.jpg" alt="" title="">
                            </div>
                            
	     
                            <div class="grid_txt">
                                    <p itemprop="name">Подлинность гарантируется </p>
                            </div>
                            <div class="clear"></div>
                        </li>
                        <li itemscope="" itemtype="http://schema.org/Article" class="grid">
                            <div class="grid_img">
                                <img src="images/news_noimage.jpg" alt="" title="">
                            </div>
                            
	     
                            <div class="grid_txt">
                                    <p itemprop="name">Быстрая доставка </p>
                            </div>
                            <div class="clear"></div>
                        </li>
                        <li itemscope="" itemtype="http://schema.org/Article" class="grid">
                            <div class="grid_img">
                                <img src="images/news_noimage.jpg" alt="" title="">
                            </div>
                            
	     
                            <div class="grid_txt">
                                    <p itemprop="name">Низкие цены </p>
                            </div>
                            <div class="clear"></div>
                        </li>
                        <li itemscope="" itemtype="http://schema.org/Article" class="grid">
                            <div class="grid_img">
                                <img src="images/news_noimage.jpg" alt="" title="">
                            </div>
                            
	     
                            <div class="grid_txt">
                                    <p itemprop="name">Точно в срок</p>
                            </div>
                            <div class="clear"></div>
                        </li>
                    </ul>
                    <div class="clear"></div>
                </div>
        <div>
           <br>
            <p class="head-title">Популярные справки</p>
               <table class="popular-docs">
                <tr>
                    <td><a href="№">Название справки</a></td>
                    <td><button class="btn btn-3 btn-3e orange">Купить за 3000 руб</button></td>
                </tr>
                <tr>
                    <td><a href="№">Название справки</a></td>
                    <td><button class="btn btn-3 btn-3e orange">Купить за 3000 руб</button></td>
                </tr>
                <tr>
                    <td><a href="№">Название справки</a></td>
                    <td><button class="btn btn-3 btn-3e orange">Купить за 3000 руб</button></td>
                </tr>
                <tr>
                    <td><a href="№">Название справки</a></td>
                    <td><button class="btn btn-3 btn-3e orange">Купить за 3000 руб</button></td>
                </tr>
                <tr>
                    <td><a href="№">Название справки</a></td>
                    <td><button class="btn btn-3 btn-3e orange">Купить за 3000 руб</button></td>
                </tr>
                <tr>
                    <td><a href="№">Название справки</a></td>
                    <td><button class="btn btn-3 btn-3e orange">Купить за 3000 руб</button></td>
                </tr>
            </table>
        </div>
	
	    </div>-->