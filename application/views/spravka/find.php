<div id="main-tables">
				<div class="sorting-btn-holder">
					<div class="sorting-btn">Выберите категорию справок</div>
				</div>
				<ul id="nav-sorting" class="tabset-1">
					<li class="active">
						<a href="#tab1" class="all active"><span>Все справки</span></a>
					</li>
					<li>
						<a href="#tab2" class="for-stud"><span>Для студентов</span></a>
					</li>
					<li>
						<a href="#tab3" class="for-child"><span>Для детей</span></a>
					</li>
					<li>
						<a href="#tab4" class="for-worker"><span>Для работников</span></a>
					</li>
					<li>
						<a href="#tab5" class="from-disp"><span>Из диспансеров</span></a>
					</li>
					<li>
						<a href="#tab6" class="analyzes"><span>Прочее</span></a>
					</li>
					<li>
						<a href="#tab7" class="other"><span>Анализы</span></a>
					</li>
				</ul>
				<div id="left-col">
						<div class="tabset-holder">
             <?=$message; ?>
        <h5 id="all">Каталог справок</h5>
        <div class="tab-content">
         <div id="tab1">
            <?php foreach ($categories as $category):?>
           
            <?php if(count($category->articles)):?>
                   
                        <table id="<?=$category->url; ?>" class="for-stud">
                      <tr>
                            <td><strong><?=$category->title;?></strong></td>
                            <td></td>
                        </tr>
                       <?php foreach($category->articles as $spravka): ?>
                        <tr>
                            <td><a href="/spravka/<?=$spravka->url;?>"><?=$spravka->title;?></a></td>
                            <td><span><?=$spravka->price;?> руб.</span></td>
                        </tr>
                        <?php endforeach;?>
                    </table>
            <?php endif;?>
        <?php endforeach; ?>
         </div>
         
          <?php $i=2; ?>
           <?php foreach ($categories as $category):?>
           
            <?php if(count($category->articles)):?>
                   
                    <div id="tab<?php echo $i;?>">
                        <table id="<?=$category->url; ?>" class="for-stud">
                      <tr>
                            <td><strong><?=$category->title;?></strong></td>
                            <td></td>
                        </tr>
                       <?php foreach($category->articles as $spravka): ?>
                        <tr>
                            <td><a href="/spravka/<?=$spravka->url;?>"><?=$spravka->title;?></a></td>
                            <td><span><?=$spravka->price;?> руб.</span></td>
                        </tr>
                        <?php endforeach;?>
                    </table>
                    </div>
            <?php endif;?>
            <?php $i++;?>
        <?php endforeach; ?>
           
        </div>
    </div>
					
				</div>
				<div id="right-col">
					<table>
						<tbody>
							<tr>
								<td colspan="2"><h5>Популярные справки</h5></td>
							</tr>
							<tr>
								<td><strong>Врачебное профессиональное<br> заключение (форма 086/у)</strong></td>
								<td><span>1200 руб.</span></td>
							</tr>
							<tr>
								<td><strong>Врачебное профессиональное<br> заключение (форма 086/у)</strong></td>
								<td><span>1200 руб.</span></td>
							</tr>
							<tr>
								<td><strong>Врачебное профессиональное<br> заключение (форма 086/у)</strong></td>
								<td><span>1200 руб.</span></td>
							</tr>
							<tr>
								<td><strong>Врачебное профессиональное<br> заключение (форма 086/у)</strong></td>
								<td><span>1200 руб.</span></td>
							</tr>
							<tr>
								<td><strong>Врачебное профессиональное<br> заключение (форма 086/у)</strong></td>
								<td><span>1200 руб.</span></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="bottom-section">
				<div class="forms-holder">
					<div class="forms-block">
						<div class="title-text">
							<h5>Возникли вопросы?</h5>
							<p>Наши специалисты  незамедлительно ответят на все интересующие Вас вопросы.</p>
						</div>
						<div class="forms-set">
							<form action="/send/maindown" method="POST">
								<fieldset class="data">
                                                                        <input type="text" name="name" placeholder="Как вас зовут" required>
									<input type="text" name="phone" placeholder="Ваш телефон" required>
									<input type="text" name="town" placeholder="Ваш город" required>
								</fieldset>
								<fieldset class="text-btn">
									<textarea name="comment" placeholder="Введите сообщение" required></textarea>
									<input type="submit" value="Отправить сообщение">
								</fieldset>
								<span>Все поля обязательны для заполнения*</span>
							</form>
						</div>
					</div>
				</div>
				<div class="map-holder">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2245.009538902292!2d37.6226715!3d55.75833649999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b54a5b9cf5e1bf%3A0xa2942a7b80729248!2z0J3QuNC60L7Qu9GM0YHQutCw0Y8g0YPQuy4sIDE30YEzLCDQnNC-0YHQutCy0LAsINCg0L7RgdGB0LjRjywgMTA5MDEy!5e0!3m2!1sru!2sua!4v1444221027751" width="600" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>