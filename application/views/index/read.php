<div class="row content-inner">
    <?php if ($article): ?>
        <h2><?= $article->title; ?></h2>
        <p><?= $article->text; ?></p>
    <?php endif; ?>
</div>