<div class="c_left">
    <p class="head-title">Популярные справки</p>
    <table class="popular-docs">
        <tr>
            <td><a href="№">Название справки</a></td>
            <td><button class="btn btn-3 btn-3e orange">Купить за 3000 руб</button></td>
        </tr>
        <tr>
            <td><a href="№">Название справки</a></td>
            <td><button class="btn btn-3 btn-3e orange">Купить за 3000 руб</button></td>
        </tr>
        <tr>
            <td><a href="№">Название справки</a></td>
            <td><button class="btn btn-3 btn-3e orange">Купить за 3000 руб</button></td>
        </tr>
        <tr>
            <td><a href="№">Название справки</a></td>
            <td><button class="btn btn-3 btn-3e orange">Купить за 3000 руб</button></td>
        </tr>
        <tr>
            <td><a href="№">Название справки</a></td>
            <td><button class="btn btn-3 btn-3e orange">Купить за 3000 руб</button></td>
        </tr>
        <tr>
            <td><a href="№">Название справки</a></td>
            <td><button class="btn btn-3 btn-3e orange">Купить за 3000 руб</button></td>
        </tr>
    </table>
    <div class="clear"></div>
</div>
<!--
-->
<div class="c_right how-we-work">
    <div class="moduletable">
        <p class="head-title">Как мы работаем</p>

        <ul class="grid_bg latestnews">
            <li class="grid">
                <div class="grid_img">
                    <img src="/assets/images/news_noimage.jpg" alt="" title="">
                </div>
                <!--
                -->
                <div class="grid_txt">
                    <p itemprop="name">Подлинность гарантируется </p>
                </div>
                <div class="clear"></div>
            </li>
            <li itemscope="" itemtype="http://schema.org/Article" class="grid">
                <div class="grid_img">
                    <img src="/assets/images/news_noimage.jpg" alt="" title="">
                </div>
                <!--
                -->
                <div class="grid_txt">
                    <p itemprop="name">Быстрая доставка </p>
                </div>
                <div class="clear"></div>
            </li>
            <li itemscope="" itemtype="http://schema.org/Article" class="grid">
                <div class="grid_img">
                    <img src="/assets/images/news_noimage.jpg" alt="" title="">
                </div>
                <!--
                -->
                <div class="grid_txt">
                    <p itemprop="name">Низкие цены </p>
                </div>
                <div class="clear"></div>
            </li>
            <li itemscope="" itemtype="http://schema.org/Article" class="grid">
                <div class="grid_img">
                    <img src="/assets/images/news_noimage.jpg" alt="" title="">
                </div>
                <!--
                -->
                <div class="grid_txt">
                    <p itemprop="name">Точно в срок</p>
                </div>
                <div class="clear"></div>
            </li>
        </ul>
        <div class="clear"></div>
    </div>

</div>
<div class="moduletable ref-example">
    <h3 class=" head-title">Отзывы</h3>
    <div class="custom ref-example relative">
        <div class="next"></div>
        <div class="prev"></div>
        <div id="carousel1" class="spravochki reviews owl-carousel owl-theme">
            <div class="item review">
                <div class="review-photo">
                    <img src="/assets/images/news_noimage.jpg" alt="">
                </div>
                <div class="review-info">
                    <div class="review-info-main">
                        <p class="review-title">Отзыв 1</p>
                        <p class="review-text">Какая-то важная фраза</p>
                    </div>
                    <p class="review-info-subtitle">Имя, профессия</p>
                </div>
            </div>
            <div class="item review">
                <div class="review-photo">
                    <img src="/assets/images/news_noimage.jpg" alt="">
                </div>
                <div class="review-info">
                    <div class="review-info-main">
                        <p class="review-title">Отзыв 1</p>
                        <p class="review-text">Какая-то важная фраза</p>
                    </div>
                    <p class="review-info-subtitle">Имя, профессия</p>
                </div>
            </div>
            <div class="item review">
                <div class="review-photo">
                    <img src="/assets/images/news_noimage.jpg" alt="">
                </div>
                <div class="review-info">
                    <div class="review-info-main">
                        <p class="review-title">Отзыв 1</p>
                        <p class="review-text">Какая-то важная фраза</p>
                    </div>
                    <p class="review-info-subtitle">Имя, профессия</p>
                </div>
            </div>
            <div class="item review">
                <div class="review-photo">
                    <img src="/assets/images/news_noimage.jpg" alt="">
                </div>
                <div class="review-info">
                    <div class="review-info-main">
                        <p class="review-title">Отзыв 1</p>
                        <p class="review-text">Какая-то важная фраза</p>
                    </div>
                    <p class="review-info-subtitle">Имя, профессия</p>
                </div>
            </div>
            <div class="item review">
                <div class="review-photo">
                    <img src="/assets/images/news_noimage.jpg" alt="">
                </div>
                <div class="review-info">
                    <div class="review-info-main">
                        <p class="review-title">Отзыв 1</p>
                        <p class="review-text">Какая-то важная фраза</p>
                    </div>
                    <p class="review-info-subtitle">Имя, профессия</p>
                </div>
            </div>
            <div class="item review">
                <div class="review-photo">
                    <img src="/assets/images/news_noimage.jpg" alt="">
                </div>
                <div class="review-info">
                    <div class="review-info-main">
                        <p class="review-title">Отзыв 1</p>
                        <p class="review-text">Какая-то важная фраза</p>
                    </div>
                    <p class="review-info-subtitle">Имя, профессия</p>
                </div>
            </div>
            <div class="item review">
                <div class="review-photo">
                    <img src="/assets/images/news_noimage.jpg" alt="">
                </div>
                <div class="review-info">
                    <div class="review-info-main">
                        <p class="review-title">Отзыв 1</p>
                        <p class="review-text">Какая-то важная фраза</p>
                    </div>
                    <p class="review-info-subtitle">Имя, профессия</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="moduletable">
    <p class="head-title">Медицинские справки в Москве</p>


    <div class="custom">
        <p>Каждый человек, проживающий в России знает, что такое бюрократия. Различного характера справки требуются практически везде, но медицинские - значительно чаще, чем хотелось бы. Контроль здоровья граждан необходим, ведь государство несет ответственность за здоровье нации, однако времени и сил посещение докторов занимает немало.</p>
        <p>Обследования, анализы, осмотры - поликлиники изо дня в день кишат больными и здоровыми пациентами, нервно ожидающими свою очередь. И никто не сможет поспорить с тем, что получение медицинской справки - дело очень и очень хлопотное.</p>
        <p>Ритм современной жизни значительно ускорился и время беспощадно летит вперед. У большинства работающих людей не найдется и минутки даже на заслуженный отдых, не говоря уже о беготне по поликлиникам и диспансерам.</p>
        <p>&nbsp;</p>
        <p>В свете сложившейся ситуации мы предлагаем Вам альтернативный выход из бюрократической паутины - оформление справок медицинского характера с помощью нашей компании.</p>
        <p>&nbsp;</p>
        <p>Понимая, что большинство жителей России не располагают достаточным количеством времени для получения той или иной справки, мы с радостью оказываем помощь в оформлении необходимых справок своим клиентам.</p>
        <p>Конечно, у каждого человека свои причины, по которым он не имеет возможности заняться самостоятельным оформлением медицинской документации, и мы это понимает. Так же, нередко случается, что справка нужна задним числом или наперед - мы всегда готовы пойти навстречу.</p>
        <p>Наша компания сотрудничает с большим перечнем поликлиник, диспансеров и прочих медучреждений, высококвалифицированные специалисты которых имеют многолетний опыт в оформлении медицинских справок.</p>
        <p>Спектр наших услуг охватывает оформление абсолютно любых медицинских справок, начиная направлением на массаж и заканчивая пакетом справок, необходимых для академического отпуска.</p>
        <p>&nbsp;</p>
        <p>Все готовые документы, которыми мы обеспечиваем своих клиентов, являются абсолютно легальными, и, что очень важно, строго соответствуют требованиям законодательства, отвечая при этом установленным государственным стандартам.</p>
        <p>Все оформляемые нами медицинские справки имеют необходимые степени защиты бланка, записи заверяются подлинными подписями квалифицированных медиков, а так же всеми необходимыми печатями и штампами.</p>
        <p>Купить медицинскую справку у нас очень легко. Эта процедура займет всего несколько минут, при этом Вам даже не потребуется выходить из дома.</p>
        <p>Для того, чтоб купить медсправку у нас на сайте, достаточно позвонив или отправив смс сообщение нашему консультанту по номеру: 8 (965)409-91-97</p>
        <p>Или же заказать обратный звонок, отправив смс сообщения на следующие номера:</p>
        <p>&nbsp;</p>
        <p>Для абонентов <strong>Megafon: *144#+79654099197</strong>;</p>
        <p>Для абонентов <strong>Beeline: *144#+79654099197</strong>;</p>
        <p>Для абонентов <strong>MTC: *144#+79654099197</strong>;</p>
        <p>&nbsp;</p>
        <p>Так же на нашем сайте есть форма обратной связи, после заполнения которой в ближайшее наши специалисты свяжутся с Вами, для подробного обсуждения деталей.</p>
        <p>Или же можно воспользоваться круглосуточным чатом, работающим в режиме онлайн, посредством которого наши операторы с удовольствием ответят на все Ваши вопросы.</p>
    </div>
</div>