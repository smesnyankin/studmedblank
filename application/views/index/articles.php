<?php if(count($pages)){ 
    $j=1;
    foreach($pages as $pag){ ?>
        <a href="/stati/<?=$pag->id; ?>"><?=$j; ?></a>
        <?php $j++;
    }
 } ?>

<div id="articles">
    <div class="articles-blocks">
       <h5>Статьи</h5>
        <?php if ($page): ?>
        <?= $page->title; ?>
        <?php if (count($articles)): ?>
        <div class="articles-holder">
            <?php foreach ($articles as $article): ?>
                       <div class="article">
                            <div class="img-holder">
                                <img src="/uploads/stati/<?= is_file($_SERVER['DOCUMENT_ROOT'] . '/uploads/stati/' . $article->img ) ? $article->img  : 'preview.jpg';?>" alt="Foto">
                            </div>
                            <div class="description-block">
                                <h6><?= $article->title; ?></h6>
                                <p><?=previewText($article->text, 200);?></p>
                                <a href="/spravka/<?= $article->go_url; ?>">ЧИТАТЬ ДАЛЕЕ</a>
                            </div>
                        </div>
            <?php endforeach; ?>
        </div>
        <?php endif; ?>
        <?php

     endif?>
    </div>
</div>