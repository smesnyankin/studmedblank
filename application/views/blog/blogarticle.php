<div class="c_left">
		
					<div id="system-message-container">
	</div>

			<div class="item-page" itemscope="" itemtype="http://schema.org/Article">
	<meta itemprop="inLanguage" content="ru-RU">
	
		
			<div class="page-header">
		<h2 itemprop="name" class="head-title">
							Справка вызов на сессию/дипломную по цене 6000 руб				</h2>
							</div>
	
	
	
		
								<div class="articleBody">
								<img src="images/001_s.jpg" alt="">
		<p>Справка-вызов на сессию/дипломную – это документ, который является официальным приглашением студента высшего учебного заведения для сдачи сессии, защиту дипломной работы и т.д.&nbsp; Она предъявляется человеком на том предприятии, на котором он официально работает. Выдаётся данная справка только в случае, если сотрудник того или иного предприятия является студентом-заочником ВУЗа. Законодательство Российской Федерации способствует получению своими гражданами высшего образования и оказывает им помощь в виде времени для подготовки и сдачи экзаменов, а также прохождения других форм учебного контроля.</p>
<p style="margin-top: 10px; font-family: Arial, sans-serif; font-size: 14px; font-style: normal; font-variant: normal; line-height: 20px; color: #333333;">Форма предоставления времени для прохождения форм учебного контроля в ВУЗе состоит в следующем. При получении студентом-заочником или начальником предприятия, на котором он официально трудоустроен, справки-вызова, сотрудник этого предприятия пишет заявление на учебный отпуск. После этого начальник обязан подписать заявление и, таким образом, предоставить работнику учебный отпуск, длительность которого может варьироваться от 1 до 4 месяцев. Длительность отпуска определяется в зависимости от причины, по которой студента-заочника вызывают в высшее учебное заведение: если его вызывают для сдачи сессии, то продолжительность учебного отпуска будет составлять 1 месяц, в случае же, если он вызывается для сдачи госэкзаменов, то продолжительность отпуска составит 3-4 месяца. Вот, собственно и всё, такова форма предоставления учебного отпуска.</p>
<p style="margin-top: 10px; font-family: Arial, sans-serif; font-size: 14px; font-style: normal; font-variant: normal; line-height: 20px; color: #333333;">Важным аспектом учебного отпуска, предоставляемого на основании справки-вызова, является то, что предприятие, на котором работает студент-заочник, этот отпуск оплачивает. Причём оплачивается он независимости от длительности. Согласитесь, это положительный аспект. Таким образом, наше государство заботится о том, чтобы работающие студенты-заочники беспрепятственно получать высшее образование, не теряя заработной платы и нарабатывая рабочий стаж. Закон в этой ситуации исключительно на стороне студента: если он официально трудоустроен на предприятии, то его начальник отказать ему в предоставлении учебного отпуска не может ни под каким предлогом. Вообще же, получение работником высшего образования является выгодным аспектом для начальника предприятия, поскольку закончив высшее учебное заведение, работник станет квалифицированным специалистом.</p>
<p>&nbsp;</p> 	</div>

	
						</div>

				
		
		<div class="clear"></div>
	    </div><!--
	    --><div class="c_right">
				<div class="moduletable">
                    <p class="head-title">Как мы работаем</p>

                    <ul class="grid_bg latestnews">
                        <li class="grid">
                            <div class="grid_img">
                                <img src="images/news_noimage.jpg" alt="" title="">
                            </div>
                            <!--
	     -->
                            <div class="grid_txt">
                                    <p itemprop="name">Подлинность гарантируется </p>
                            </div>
                            <div class="clear"></div>
                        </li>
                        <li itemscope="" itemtype="http://schema.org/Article" class="grid">
                            <div class="grid_img">
                                <img src="images/news_noimage.jpg" alt="" title="">
                            </div>
                            <!--
	     -->
                            <div class="grid_txt">
                                    <p itemprop="name">Быстрая доставка </p>
                            </div>
                            <div class="clear"></div>
                        </li>
                        <li itemscope="" itemtype="http://schema.org/Article" class="grid">
                            <div class="grid_img">
                                <img src="images/news_noimage.jpg" alt="" title="">
                            </div>
                            <!--
	     -->
                            <div class="grid_txt">
                                    <p itemprop="name">Низкие цены </p>
                            </div>
                            <div class="clear"></div>
                        </li>
                        <li itemscope="" itemtype="http://schema.org/Article" class="grid">
                            <div class="grid_img">
                                <img src="images/news_noimage.jpg" alt="" title="">
                            </div>
                            <!--
	     -->
                            <div class="grid_txt">
                                    <p itemprop="name">Точно в срок</p>
                            </div>
                            <div class="clear"></div>
                        </li>
                    </ul>
                    <div class="clear"></div>
                </div>
        <div>
           <br>
            <p class="head-title">Популярные справки</p>
               <table class="popular-docs">
                <tr>
                    <td><a href="№">Название справки</a></td>
                    <td><button class="btn btn-3 btn-3e orange">Купить за 3000 руб</button></td>
                </tr>
                <tr>
                    <td><a href="№">Название справки</a></td>
                    <td><button class="btn btn-3 btn-3e orange">Купить за 3000 руб</button></td>
                </tr>
                <tr>
                    <td><a href="№">Название справки</a></td>
                    <td><button class="btn btn-3 btn-3e orange">Купить за 3000 руб</button></td>
                </tr>
                <tr>
                    <td><a href="№">Название справки</a></td>
                    <td><button class="btn btn-3 btn-3e orange">Купить за 3000 руб</button></td>
                </tr>
                <tr>
                    <td><a href="№">Название справки</a></td>
                    <td><button class="btn btn-3 btn-3e orange">Купить за 3000 руб</button></td>
                </tr>
                <tr>
                    <td><a href="№">Название справки</a></td>
                    <td><button class="btn btn-3 btn-3e orange">Купить за 3000 руб</button></td>
                </tr>
            </table>
        </div>
	
	    </div>
