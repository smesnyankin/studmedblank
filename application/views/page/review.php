<div id="reviews">
	<div class="reviews-blocks">
		<h1><?=$page->title; $i=0; ?></h1>
		<ul>
            <?php foreach($reviews as $review){ $i++; ?>
			<li>
				<div class="description">
					<div class="text"><?=$review->text; ?></div>
				</div>
				<div class="img-holder">
					<?php if ($review->img): ?>
                        <img src="/uploads/reviews/<?=$review->img; ?>" alt="foto" width="100" height="100">
                    <?php else: ?>
                        <img src="/assets/images/no-photo.jpg" alt="foto">
                    <?php endif; ?> 
				</div>
				<div class="user-info">
					<p><?=$review->specialization; ?> <span><?=$review->city; ?>, </span></p>
					<strong><?=$review->nik; ?></strong>
				</div>
			</li>
		<?php if ($i===2){ ?>
			<li class="add-review">
				<div class="description-free">
					<div class="text">Напишите ваш отзыв уже сейчас!</div>
				</div>
				<div class="img-holder-free">
					<img src="/assets/images/ico-12.png" alt="foto">
				</div>
				<div class="popup-holder-2">
					<div class="popup">
						<div class="forms-block">
							<div class="title-text">
								<h4>Отправить отзыв</h4>
								<p>Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что он у себя в постели превратился в страшное насекомое. Лежа на</p>
							</div>
							<div class="forms-set">
								<form action="#">
									<fieldset class="data">
										<div class="add-foto"><img src="/assets/images/ico-12.png" alt="foto"></div>
										<div class="male">!</div>
										<div class="female">!</div>
										<strong>Нажмите на крестик чтобы загрузить фото.</strong>
										<input type="text" name="name" placeholder="Как вас зовут?">
										<input type="text" name="phone" placeholder="Где Вы живете?">
										<input type="text" name="town" placeholder="Ваша специальность">
									</fieldset>
									<fieldset class="text-btn">
										<textarea placeholder="Напишите отзыв"></textarea>
										<input type="submit" value="Отправить отзыв">
									</fieldset>
									<span>Все поля обязательны для заполнения*</span>
								</form>
							</div>
						</div>
					</div>
					<div class="closer-for-popup"></div>
				</div>
				<div class="user-info">
					<p>Нажмите на крестик чтобы<br> начать писать отзыв.</p>
				</div>
			</li>
                    <?php } 
                    } ?>		
		</ul>
	</div>
	<div class="pagination">
		<?php if(count($pages)){ 
		    $j=1;
		    foreach($pages as $pag){ ?>
		        <a href="/page/review/<?=$pag->id; ?>"><?=$j; ?></a>
		        <?php $j++;
		    }
		 } ?>
	</div>
	<script>
	// 	$(document).ready(function(){
	// 		if('.pagination a[href=/page/review/1]' || ''){
	// 			$('.pagination a').addClass('active');
	// 		} else{
	// 			$('.pagination a').removeClass('active');
	// 		}
	// 		// $('.pagination a').click(function(){
	// 		// 	$(this).addClass('active');
	// 		// });
	// 	});
	// </script>
</div>