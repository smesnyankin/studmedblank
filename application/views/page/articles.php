<div id="articles">
    <div class="articles-blocks">
       <h5>Статьи</h5>
        <?php if ($page): ?>
        <div class="add-title">
            <?= $page->title; ?>
        </div>
        <?php if (count($articles)): ?>
        <div class="articles-holder">
            <?php foreach ($articles as $article): ?>
                <div class="article">
                    <div class="img-holder">
                        <img src="/uploads/stati/<?= is_file($_SERVER['DOCUMENT_ROOT'] . '/uploads/stati/' . $article->img ) ? $article->img  : 'preview.jpg';?>" alt="Foto">
                    </div>
                    <div class="description-block">
                        <h6><?= $article->title; ?></h6>
                        <p class="text"><?=previewText($article->text, 200);?></p>
                        <a href="/<?= $article->go_url; ?>">Читать далее</a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <?php endif; ?>
        <?php
     endif?>
    </div>
    <div class="pagination">
        <?php if(count($pages)){ 
            $j=1;
            foreach($pages as $pag){ ?>
                <a href="/page/articles/<?=$pag->id; ?>"><?=$j; ?></a>
                <?php $j++;
            }
         } ?>
    </div>
</div>
