<div id="rules-filling">
	<div class="rules-blocks">
		<h5><?=$page->title; ?></h5>
		<p><?=$page->text; ?></p>
         <?php foreach($rules as $rule){ ?>
             <ul>
				<li class="img-sertificate">
					<a class="lightbox" href="#fancybox3">
						<img src="/uploads/rules/<?=$rule->img; ?>" alt="Sertificate">
					</a>
					<div class="fancybox-holder">
						<div id="fancybox3" class="lightbox">
							<img src="/uploads/rules/<?=$rule->img; ?>" alt="Sertificate">
						</div>
					</div>
				</li>
				<li class="rulers-description">
					<div class="title-block">
						<h5><?=$rule->spravka_title; ?></h5>
						<p><?=$rule->spravka_text; ?></p>
					</div>
					<div class="rulers-list">
						<h5><?=$rule->rule_title; ?></h5>
						<span><?=$rule->rule_title; ?></span>
						<?=$rule->rule_text; ?>
					</div>
				</li>
			</ul>
        <?php } ?>
	</div>
	<div class="pagination">
		<?php if(count($pages)>1){ 
		    $j=1;
		    foreach($pages as $pag){ ?>
		        <a href="/page/rules/<?=$pag->id; ?>"><?=$j; ?></a>
		        <?php $j++;
		    }
		 } ?>
	</div>
</div>