<div id="question-answer">
    <div class="question-answer-blocks">
        <h5>Вопрос-ответ</h5>
        <?php if ($page): ?>
        <?= $page->title; ?><br>
        <?php if (count($faqs)): ?>
            <div>
                <?php foreach ($faqs as $faq):  ?>
                    <ul>
                        <li class="question">
                            <p><b><?= $faq->question; ?></b></p>
                        </li>
                        <li class="answer">
                            <p><?= $faq->answer; ?></p>
                        </li>
                    </ul>
                <?php endforeach; ?>
            </div>
            <?php endif; ?>
            <?php endif; ?>
    </div>
    <div class="pagination">
        <?php if(count($pages)){ 
            $i=1;
            foreach($pages as $page){ ?>
                <a href="/page/faq/<?=$page->id; ?>"><?=$i; ?></a>
                <?php $i++;
            }
         } ?>
    </div>
</div>
