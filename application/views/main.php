<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?= $this->meta_title; ?></title>
        <link href="http://project1.binovery.com/templates/prs/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">
        <link rel="stylesheet" href="/assets/css/template.css" type="text/css">
        <link rel="stylesheet" href="/assets/css/owl.carousel.css" type="text/css">
        <link rel="stylesheet" href="/assets/css/owl.theme.css" type="text/css">
        <script src="/assets/js/jquery.min.js" type="text/javascript"></script>
        <script src="/assets/js/jquery-noconflict.js" type="text/javascript"></script>
        <script src="/assets/js/jquery-migrate.min.js" type="text/javascript"></script>
        <script src="/assets/js/caption.js" type="text/javascript"></script>
        <script src="/assets/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="/assets/js/owl.carousel.min.js" type="text/javascript"></script>
        <script src="/assets/js/template.js" type="text/javascript"></script>
        <script type="text/javascript">
            jQuery(window).on('load', function () {
                new JCaption('img.caption');
            });
        </script>
    </head>
    <body>
        <?php include dirname(__FILE__) . '/layouts/' . $this->layout . '.php'; ?>

        <!-- BEGIN JIVOSITE CODE {literal} -->
        <!--
        <script type="text/javascript">
        (function(){ var widget_id = 'IMDtja2DA4';
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();
        </script>
        -->
        <!-- {/literal} END JIVOSITE CODE -->
        <!--
        <script>
    
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
    
    
        ga('create', 'UA-21845411-3', 'auto');
    
        ga('send', 'pageview');
    
    
    
        </script>
        -->
        <!-- Yandex.Metrika counter -->
        <!--
        <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter29822199 = new Ya.Metrika({id:29822199,
                            webvisor:true,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true});
                } catch(e) { }
            });
    
            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
    
            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
        </script>
        <noscript>&lt;div&gt;&lt;img src="//mc.yandex.ru/watch/29822199" style="position:absolute; left:-9999px;" alt="" /&gt;&lt;/div&gt;</noscript>
        -->
        <!-- /Yandex.Metrika counter -->

    </body>
</html>
