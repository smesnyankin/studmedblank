<div class="wrap">
    <div class="section1">
        <div class="header">
            <div class="logo">
                <a class="middle" href="index.html"><img src="/assets/images/logo.png" alt="studmedblank"><span class="site-title middle" title="studmedblank">Studmedblank <br>Медицинские справки в Москве</span> </a>
            </div>
            <!--
            
                            <div class="doctor"></div>
            -->
            <div class="order-head">
                <p class="order-head-title">Заголовок</p>
                <p>Lorem ipsum dolor sit amet.</p>
                <button class="btn btn-3 btn-3e orange open-popup">Купить справку</button>

            </div>
            <div class="callback">
                <ul>
                    <li>
                        <img src="/assets/images/beeline.png" title="" alt="">
                        <p>*144#+79654099197</p>
                    </li>
                    <li>
                        <img src="/assets/images/mts.png" title="" alt="">
                        <p>*110*89654099197</p>
                    </li>
                    <li>
                        <img src="/assets/images/megafon.png" title="" alt="">
                        <p>*144#+79654099197</p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="menu adaptive-menu">
            <div class="moduletable middle top-menu">
                <a class="menu-button">≡ Меню</a>
                <ul class="nav menu_qq">
                    <li class="item-101 current active"><a href="/">Главная</a></li>
                    <li class="item-102"><a href="/vse-spravki">Справки и цены</a></li>
                    <li class="item-105"><a href="/page/articles">Статьи</a></li>
                    <!--<li class="item-109"><a href="/feed">Обратная связь</a></li>-->
                    <li class="item-103"><a href="/contacts">Контакты</a></li>
                </ul>
            </div>
            <div class="search">
                <form action="">
                    <input type="text" placeholder="Что ищете?">
                    <input type="submit">
                </form>
            </div>

        </div>
    </div>
    <div class="content">
        <div class="breadcrumbs">
            <p class="<?php if ($this->getBreadCrumbs()): ?> breadcrumbs <?php endif; ?>">
                <?php if ($this->getBreadCrumbs()): ?>
                    <?= $this->getBreadCrumbs(); ?>
                <?php endif; ?>
            </p>
        </div>
        <?= $content; ?>
            <!--<p class="breadcrumbs">Главная / Хлебная / Крошка</p>-->
    </div>		


    <div class="footer clearfix">
        <div class="header">
            <div class="logo">
                <a class="middle" href="index.html"><img src="/assets/images/logo.png" alt="studmedblank"><span class="site-title middle" title="studmedblank">Studmedblank <br>Медицинские справки в Москве</span> </a>
            </div>

            <div class="order-head relative">
                <p class="order-head-title">Поиск</p>
                <p>Lorem ipsum dolor sit amet.</p>
                <div class="search">
                    <form action="">
                        <input type="text" placeholder="Что ищете?">
                        <input type="submit">
                    </form>
                </div>

            </div>
            <div class="callback">
                <ul>
                    <li>
                        <img src="/assets/images/beeline.png" title="" alt="">
                        <p>*144#+79654099197</p>
                    </li>
                    <li>
                        <img src="/assets/images/mts.png" title="" alt="">
                        <p>*110*89654099197</p>
                    </li>
                    <li>
                        <img src="/assets/images/megafon.png" title="" alt="">
                        <p>*144#+79654099197</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="bottom-dock clearfix">
        <p class="copy">2015 © copyright</p>
        <ul>
            <li><a href="#">Главная</a></li>
            <li><a href="#">Справки и цены</a></li>
            <li><a href="#">Статьи</a></li>
            <li><a href="#">Обратная связь</a></li>
            <li><a href="#">Контакты</a></li>
        </ul>
    </div>
</div>
<div class="popup-window">
    <form id="ajaxForm" class="popup-window-form" action="#">
<!--           <p>Contact us</p>-->
        <label>
            Название справки
            <input type="text" placeholder="Название справки">
        </label>
        <label>
            Номер телефона
            <input type="text" placeholder="Номер телефона">
        </label>
        <label>
            ФИО
            <input type="text" placeholder="ФИО">
        </label>

        <button class="btn btn-3 btn-3e orange open-popup">Купить справку</button>
    </form>
    <a class="popup-window-close" href="javascript:void(0)">X</a>
</div>
<div class="popup-window-overlay">&nbsp;</div>
</div>