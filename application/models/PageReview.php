<?php 
class PageReview extends ModelTable {
	static $table = 'page_review';
	public $safe = array('id', 'url', 'title', 'meta_title', 'meta_keywords', 'meta_description');
}