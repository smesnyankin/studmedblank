<?php
class BlogPost extends ModelTable {
	static $table = 'blog_post';
	public $safe = array('id', 'id_page', 'img', 'title', 'text', 'time', 'go_url', 'meta_title', 'meta_keywords', 'meta_description');
        
}