<?php

class Page extends ModelTable {

    static $table = 'pages';
    public $safe = array('id', 'url', 'title', 'meta_title', 'meta_keywords', 'meta_description', 'content', 'mod_time');

}
