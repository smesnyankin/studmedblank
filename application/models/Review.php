<?php
class Review extends ModelTable{
	static $table = 'review';
	public $safe = array('id', 'id_page', 'specialization', 'city', 'nik', 'text', 'img');
}