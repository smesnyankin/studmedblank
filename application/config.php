<?php

return array(
    'sitename' => 'Spravka',
    'db' => include 'config.db.php',
    'layout' => 'base',
    'path_error_controller' => '/error', //урла контроллера ошибки
    'router' => array(
        '([a-z0-9+_\-]+)/([a-z0-9+_\-]+)/([a-z0-9+_\-]+)' => '$controller/$action/$id',
        '([a-z0-9+_\-]+)/([a-z0-9+_\-]+)' => '$controller/$action',
        '([a-z0-9+_\-]+)' => '$controller',
        '([a-z0-9+_\-]+)\.html' => 'page/read/$id',
        'o-kompanii' => 'index/about',
//        'stati' => 'index/articles',
        // 'katalog' => 'index/catalog',
        'oplata' => 'index/pay',
        'contacts' => 'index/contactsanddelivery',
        'vse-spravki' => 'index/catalog',
        'spravka/([a-z0-9+_\-]+)' => 'spravka/view/$id',
        'spravka/search' => 'spravka/search',
        'spravka/list' => 'spravka/list',
                    //feed
                    
    // 'register' => 'user/register',
    ),
);
